<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'third_party/forms.php';
 
class Usuario extends MY_Controller {

    private $form;

    public function __construct(){
        parent::__construct();
        $this->form=new Forms(); 

        if (!$this->session->username){
            redirect('/');
        }
    }
    
    /**
     * @method: Crear Usuario
     */
    public function crear(){
        $data = $this->form->perfil($_FILES);
        if (!$this->form_validation->run()){ # Si la validacion no se ejecuto con exito
            $data['title'] = 'CREAR USUARIO';
            echo $this->blade->view()->make('admin/formulario/usuario', $data);
        }else {
            $data['rol']='1';
            $this->load->model('modelos');
            $this->modelos->usuario($data);
            redirect('admin/inicio');
        }
    }

    /**
     * @method: Modificar usuario
     */
    public function modificar($slug){
        $instance = Usuarios::where('slug', $slug)->first();
        $data = $this->form->perfil_modificar($_FILES, $instance);
        if (!$this->form_validation->run()){ # Si la validacion no se ejecuto con exito
            $data['title'] = 'MODIFICAR CUENTA';
            $data['instance'] = $instance;
            echo $this->blade->view()->make("admin/formulario/usuario_modificar", $data);
        }else {
            $sesion = array(
                'id_usuario' => $instance->id_usuario,
                'username' => $instance->username,
                'nombres' => $instance->nombres,
                'apellidos' => $instance->apellidos,
                'id_perfil' => $instance->id_perfil,
                'img' => $instance->img,
                'email' => $instance->email,
                'edad' => $instance->edad,
                'slug' => $instance->slug,
                'simple_name' => $instance->get_simple_name(),
                'full_name' => $instance->get_full_name()
            );
            $this->session->set_userdata($sesion);
            redirect('admin/inicio');
        }
    }

    /**
     * @method: Obtener todos los usuarios
     */
    public function obtener($user){
        if (in_array($user, array('usuario', 'cliente'))){
            $data['title'] = 'TABLA USUARIO';
            echo $this->blade->view()->make("admin/tabla/{$user}", $data);
        }else{
            echo 'nope';
        }
    }

    /**
     * @method: Cambar de estado a usuario
     */
    public function estado($user, $slug){
        if (in_array($user, array('usuario', 'cliente'))){
            $usuario = Usuarios::where('slug', $slug)->first();
            $usuario->estado = ($usuario->estado) ? '0' : '1' ;
            $usuario->save();
            redirect("/admin/{$user}/tabla");
        }else{
            show_404();
        }
    }

}