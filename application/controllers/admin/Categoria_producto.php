<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'third_party/forms.php';

class Categoria_producto extends MY_Controller {

    private $form;

    public function __construct(){
        parent::__construct();
        $this->form=new Forms(); 

        if (!$this->session->username){
            redirect('/');
        }
    }

    public function crear(){
        $data = $this->form->categoria();
        if (!$this->form_validation->run()){ # Si la validacion no se ejecuto con exito
            $data['title'] = 'CATEGORIA PRODUCTO';
            echo $this->blade->view()->make('admin/formulario/categoria', $data);
        }else {
            $this->load->model('modelos');
            $this->modelos->categoria_producto($data);
            redirect('admin/inicio');
        }
    }

    public function modificar($slug){
        $instance = Categoria_productos::all()->where('slug', $slug)->first();
        $data = $this->form->categoria_modificar($instance);
        if (!$this->form_validation->run()){ # Si la validacion no se ejecuto con exito
            $data['title'] = 'CATEGORIA PRODUCTO';
            $data['instance'] = $instance;
            echo $this->blade->view()->make('admin/formulario/categoria_modificar', $data);
        }else {
            $instance->nombre = $data['nombre'];
            $instance->save();
            redirect('admin/inicio');
        }
    }

    public function obtener(){
        $data['title'] = 'CATEGORIA PRODUCTO';
        $data['categoria'] = Categoria_productos::all();
        echo $this->blade->view()->make('admin/tabla/categoria_producto', $data);
    }

    public function estado($slug){
        $categoria = Categoria_productos::all()->where('slug', $slug)->first();
        $categoria->estado = ($categoria->estado) ? '0' : '1' ;
        $categoria->save();
        redirect('admin/categoria/producto/tabla');
    }

}