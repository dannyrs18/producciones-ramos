<?php

defined('BASEPATH') OR exit('No direct script access allowed');
 
class Reservacion extends MY_Controller {

    public function __construct(){
        parent::__construct();

        if (!$this->session->username){
            redirect('/');
        }
    }

    public function productos($id, int $navigation){
        $pagina = array_chunk(Productos::all()->where('categoria_id', $id)->toArray(), 6);
        $pedido = Pedidos::where('estado', '1')->where('cliente_id', $this->session->id)->first();
        $data = null;
        if ($pedido){
            $aux = Proformas::all()->where('pedido_id', $pedido->id);
            $data['proforma'] = array();
            foreach ($aux as $value) {
                array_push($data['proforma'], $value->producto_id);
            }
        }
        if (isset($pagina[$navigation])){
            $data['num'] = $navigation;
            $data['navigation'] = $pagina[$navigation];
            $data['total'] = count($pagina);
            //echo json_encode($data);
            echo $this->blade->view()->make('admin/reservacion/productos', $data);
        }else{
            show_404();
        }
    }

    public function servicios($id, int $navigation){
        $pagina = array_chunk(Servicios::all()->where('categoria_id', $id)->toArray(), 6);
        $pedido = Pedidos::where('estado', '1')->where('cliente_id', $this->session->id)->first();
        $data = null;
        if ($pedido){
            $aux = Proformas::all()->where('pedido_id', $pedido->id);
            $data['proforma'] = array();
            foreach ($aux as $value) {
                array_push($data['proforma'], $value->servicio_id);
            }
        }
        if (isset($pagina[$navigation])){
            $data['num'] = $navigation;
            $data['navigation'] = $pagina[$navigation];
            $data['total'] = count($pagina);
            echo $this->blade->view()->make('admin/reservacion/servicios', $data);
        }else{
            show_404();
        }
    }

    public function proforma(){
        $data['title'] = 'PROFORMA';
        $data['proforma'] = Proformas::all()->where('estado', '1')->where('cliente_id', $this->session->id);
        echo $this->blade->view()->make('admin/reservacion/proforma', $data);
    }

    public function agenda(){
        $info = array();
        $paleta = array('#f56954', '#f39c12', '#0073b7', '#00c0ef', '#00a65a', '#3c8dbc');
        foreach (Pedidos::all() as $value) {
            foreach ($value->proformas() as $key) {
                $aux = rand(0, 5);
                if ($key->producto_id){
                    array_push($info, array(
                        'title' => $key->producto()->nombre,
                        'start' => $value->fecha_inicio,
                        'end'   => $value->fecha_fin,
                        //'url'   => $key->producto()->nombre,
                        'backgroundColor' => $paleta[$aux],
                        'borderColor' => $paleta[$aux]
                    ));
                }else if ($key->servicio_id){
                    array_push($info, array(
                        'title' => $key->servicio()->nombre,
                        'start' => $value->fecha_inicio,
                        'end'   => $value->fecha_fin,
                        //'url'   => $key->producto()->nombre,
                        'backgroundColor' => $paleta[$aux],
                        'borderColor' => $paleta[$aux]
                    ));
                }
            }
        }
        echo $this->blade->view()->make('admin/reservacion/calendar', ['info' => json_encode($info)]);
    }

    public function reporte($id){
        $this->load->library('pdf');
        $this->pdf = new Pdf();
        $this->pdf->SetTitleHead('PROFORMA');
        $this->pdf->AddPage();
        $this->pdf->AliasNbPages();
        $info = array();
        $c=0;
        $i=0;
        $pedido = Pedidos::where('estado', '1')->where('cliente_id', $id)->first();
        foreach ($pedido->proformas() as $item){
            $c = $c + $item->costo;
            if ($item->producto()){
                if ($item->fecha_fin){
                    array_push($info, array(++$i, $item->cantidad, $pedido->fecha_inicio.'-'.$pedido->fecha_fin, $item->producto()->nombre, $item->producto()->costo, money_format('%.2n', $item->costo)));
                }else{
                    array_push($info, array(++$i, $item->cantidad, $pedido->fecha_inicio, $item->producto()->nombre, $item->producto()->costo, money_format('%.2n', $item->costo)));
                }
            } else {
                if ($item->fecha_fin){
                    array_push($info, array(++$i, $item->cantidad, $pedido->fecha_inicio.'-'.$pedido->fecha_fin , $item->servicio()->nombre, $item->servicio()->costo, money_format('%.2n', $item->costo)));
                }else{
                    array_push($info, array(++$i, $item->cantidad, $pedido->fecha_inicio , $item->servicio()->nombre, $item->servicio()->costo, money_format('%.2n', $item->costo)));
                }
            }
        }
        array_push($info, array('', '', '' , '', 'Total', money_format('%.2n', $c)));
        $data = array(
            'cabecera'=>array('Nro', 'Cant.', 'Fecha', 'Nombre', 'Unitario', 'Total'),
            'contenido'=>$info,
            'tamaño'=>array(15, 15, 50, 55, 25, 25)//
        );
        $this->pdf->head_table($data['cabecera'], $data['tamaño']);
        $this->pdf->SetWidths($data['tamaño']);
        $this->pdf->table($data['contenido']);
        $this->pdf->Output();
    }

    public function proforma_cliente(){
        $data['title'] = 'PROFORMAS';
        echo $this->blade->view()->make('admin/tabla/proforma_cliente', $data);
    }

    public function reservas($id){
        $pedido = Pedidos::find($id);
        $pedido->estado = '0';
        $pedido->save();
        redirect('/admin/reservacion/proforma_cliente');
    }

}