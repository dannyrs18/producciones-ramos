<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'third_party/forms.php';

class Categoria_servicio extends MY_Controller {

    private $form;

    public function __construct(){
        parent::__construct();
        $this->form=new Forms(); 

        if (!$this->session->username){
            redirect('/');
        }
    }

    public function crear(){
        $data = $this->form->categoria();
        if (!$this->form_validation->run()){ # Si la validacion no se ejecuto con exito
            $data['title'] = 'CATEGORIA SERVICIO';
            echo $this->blade->view()->make('admin/formulario/categoria', $data);
        }else {
            $this->load->model('modelos');
            $this->modelos->categoria_servicio($data);
            redirect('admin/inicio');
        }
    }

    public function modificar($slug){
        $instance = Categoria_servicios::all()->where('slug', $slug)->first();
        $data = $this->form->categoria_modificar($instance);
        if (!$this->form_validation->run()){ # Si la validacion no se ejecuto con exito
            $data['title'] = 'CATEGORIA SERVICIO';
            $data['instance'] = $instance;
            echo $this->blade->view()->make('admin/formulario/categoria_modificar', $data);
        }else {
            $instance->nombre = $data['nombre'];
            $instance->save();
            redirect('admin/inicio');
        }
    }

    public function obtener(){
        $data['title'] = 'CATEGORIA SERVICIO';
        $data['categoria'] = Categoria_servicios::all();
        echo $this->blade->view()->make('admin/tabla/categoria_servicio', $data);
    }

    public function estado($slug){
        $categoria = Categoria_servicios::all()->where('slug', $slug)->first();
        $categoria->estado = ($categoria->estado) ? '0' : '1' ;
        $categoria->save();
        redirect('admin/categoria/servicio/tabla');
    }

}