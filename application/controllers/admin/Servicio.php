<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'third_party/forms.php';
 
class Servicio extends MY_Controller {

    private $form;

    public function __construct(){
        parent::__construct();
        $this->form=new Forms(); 

        if (!$this->session->username){
            redirect('/');
        }
    }
    
    public function crear(){
        $data = $this->form->servicio($_FILES);
        if (!$this->form_validation->run()){ # Si la validacion no se ejecuto con exito
            $data['title'] = 'CREAR SERVICIO';
            $data['categoria'] = Categoria_servicios::all()->where('estado', '1');
            echo $this->blade->view()->make('admin/formulario/servicio', $data);
        }else {
            $this->load->model('modelos');
            $this->modelos->servicio($data);
            redirect('admin/inicio');
        }
    }

    public function modificar($slug){
        $instance = Servicios::where('slug', $slug)->first();
        $data = $this->form->servicio_modificar($_FILES, $instance);
        if (!$this->form_validation->run()){
            $data['title'] = 'MODIFICAR PRODUCTO';
            $data['instance'] = $instance;
            $data['categoria'] = Categoria_servicios::all()->where('estado', '1');
            echo $this->blade->view()->make('admin/formulario/servicio_modificar', $data);
        }else {
            $instance->costo = $data['costo'];
            $instance->descripcion = $data['descripcion'];
            if ($data['nombre']){
                $instance->nombre = $data['nombre'];
            }
            if ($data['img']){
                $instance->img = $data['img'];
            }
            $instance->categoria_id = $data['categoria_id'];
            $instance->save();
            redirect('/admin/inicio');
        }
    }

    public function obtener(){
        $data['title'] = 'CREAR SERVICIO';
        echo $this->blade->view()->make('admin/tabla/servicio', $data);
    }

    public function estado($slug){
        $producto = Servicios::where('slug', $slug)->first();
        $producto->estado = ($producto->estado) ? '0' : '1' ;
        $producto->save();
        redirect("/admin/producto/tabla");
    }

    public function reporte(){
        $this->load->library('pdf');
        $this->pdf = new Pdf();
        $this->pdf->SetTitleHead('SERVICIOS');
        $this->pdf->AddPage();
        $this->pdf->AliasNbPages();
        $info = array();
        $i=0;
        foreach (Servicios::all() as $item){
            array_push($info, array(++$i, $item->categoria_producto()->nombre, $item->nombre, $item->costo, ($item->estado) ? 'Activo' : 'Inactivo'));
        }
        $data = array(
            'cabecera'=>array('Nro', 'Tipo', 'Nombre', 'Costo', 'Estado'),
            'contenido'=>$info,
            'tamaño'=>array(15, 40, 60, 35, 35)
        );
        $this->pdf->head_table($data['cabecera'], $data['tamaño']);
        $this->pdf->SetWidths($data['tamaño']);
        $this->pdf->table($data['contenido']);
        $this->pdf->Output();
    }
}