<?php
defined('BASEPATH') OR exit('No direct script access allowed');
 
class Page extends MY_Controller {

    public function __construct(){
        parent::__construct();

        if (!$this->session->username){
            redirect('/');
        }
    }
    
    /**
     * @method: Paginas de inicio del admin
     */
    public function inicio(){
        echo $this->blade->view()->make('admin/inicio');
    }

    /**
	 * @method: Cerrar secion del admin
	 */
	public function logout(){
		$this->session->sess_destroy();
		redirect ('/');
    }

    /**
     * @method: Respaldos
     */
    public function respaldo(){
        $this->load->dbutil();

        $prefs = array(     
            'format'      => 'zip',             
            'filename'    => 'produccionesramos.sql'
            );

        $backup =& $this->dbutil->backup($prefs); 

        $db_name = 'backup-on-'. date("Y-m-d-H-i-s") .'.zip';
        $save = 'pathtobkfolder/'.$db_name;

        $this->load->helper('file');
        write_file($save, $backup); 


        $this->load->helper('download');
        force_download($db_name, $backup);
    }
}