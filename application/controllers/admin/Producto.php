<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'third_party/forms.php';
 
class Producto extends MY_Controller {

    private $form;

    public function __construct(){
        parent::__construct();
        $this->form=new Forms(); 

        if (!$this->session->username){
            redirect('/');
        }
    }
    
    public function crear(){
        $data = $this->form->producto($_FILES);
        if (!$this->form_validation->run()){ # Si la validacion no se ejecuto con exito
            $data['title'] = 'CREAR PRODUCTO';
            $data['categoria'] = Categoria_productos::all()->where('estado', '1');
            echo $this->blade->view()->make('admin/formulario/producto', $data);
        }else {
            $this->load->model('modelos');
            $this->modelos->producto($data);
            redirect('admin/inicio');
        }
    }

    public function modificar($slug){
        $instance = Productos::where('slug', $slug)->first();
        $data = $this->form->producto_modificar($_FILES, $instance);
        if (!$this->form_validation->run()){
            $data['title'] = 'MODIFICAR PRODUCTO';
            $data['instance'] = $instance;
            $data['categoria'] = Categoria_productos::all()->where('estado', '1');
            echo $this->blade->view()->make('admin/formulario/producto_modificar', $data);
        }else {
            $instance->costo = $data['costo'];
            $instance->descripcion = $data['descripcion'];
            $instance->stock = $data['stock'];
            if ($data['nombre']){
                $instance->nombre = $data['nombre'];
            }
            if ($data['img']){
                $instance->img = $data['img'];
            }
            $instance->categoria_id = $data['categoria_id'];
            $instance->save();
            redirect('/admin/inicio');
        }
    }

    public function obtener(){
        $data['title'] = 'CREAR PRODUCTO';
        echo $this->blade->view()->make('admin/tabla/producto', $data);
    }

    public function estado($slug){
        $producto = Productos::where('slug', $slug)->first();
        $producto->estado = ($producto->estado) ? '0' : '1' ;
        $producto->save();
        redirect("/admin/producto/tabla");
    }

    public function reporte(){
        $this->load->library('pdf');
        $this->pdf = new Pdf();
        $this->pdf->SetTitleHead('PRODUCTOS');
        $this->pdf->AddPage();
        $this->pdf->AliasNbPages();
        $info = array();
        $i=0;
        foreach (Productos::all() as $item){
            array_push($info, array(++$i, $item->categoria_producto()->nombre, $item->nombre, $item->costo, ($item->estado) ? 'Activo' : 'Inactivo'));
        }
        $data = array(
            'cabecera'=>array('Nro', 'Tipo', 'Nombre', 'Costo', 'Estado'),
            'contenido'=>$info,
            'tamaño'=>array(15, 40, 60, 35, 35)
        );
        $this->pdf->head_table($data['cabecera'], $data['tamaño']);
        $this->pdf->SetWidths($data['tamaño']);
        $this->pdf->table($data['contenido']);
        $this->pdf->Output();
    }
}