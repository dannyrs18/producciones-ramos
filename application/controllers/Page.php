<?php
defined('BASEPATH') OR exit('No direct script access allowed');
include APPPATH . 'third_party/forms.php';

class Page extends MY_Controller {

	/**
     * @method paginas de informacion
     * @param page: nombre de la plantilla basica
     */
    public function index($page='inicio'){
        if (!file_exists(APPPATH.'views/page/'.$page.'.blade.php')){
            show_404();
        }
        if ($page != 'typo'){
            $data = array(
                'page' => $page
            );
            echo $this->blade->view()->make('page/'.$page, $data);
        }else{
            redirect('page/login');
        }
    }

    /**
     * @method Pagina de login
     * @param null
     */
    public function registro(){
        $form = new Forms(); 
        $data = $form->perfil($_FILES);
        if (!$this->form_validation->run()){ # Si la validacion no se ejecuto con exito
            echo $this->blade->view()->make('page/formulario/registro');
        }else {
            $data['rol']='2';
            $this->load->model('modelos');
            $this->modelos->usuario($data);
            redirect('/');
        }
    }

    /**
     * @method Pagina de registro
     * @param null
     */
    public function login(){
		$this->form_validation->set_rules('username', 'Usuario', 'trim|required');
		$this->form_validation->set_rules('password', 'Contraseña', 'trim|required');
		# Validation
		if (!$this->form_validation->run()){
			echo $this->blade->view()->make('page/formulario/login');
		}else{
            $user = Usuarios::where('username', $this->input->post('username'))->where('estado','1')->first();
			if ($user){
				if(password_verify($this->input->post('password'), $user->password)){
					$sesion = array(
						'id'=> $user->id,
                        'username' => $user->username,
                        'nombres' => $user->nombres,
                        'apellidos' => $user->apellidos,
                        'img' => $user->img,
                        'email' => $user->email,
                        'edad' => $user->edad,
                        'slug' => $user->slug,
                        'rol' => $user->rol,
                        'simple_name' => $user->get_simple_name(),
                        'full_name' => $user->get_full_name()
					);
					$this->session->set_userdata($sesion);
                    redirect('admin/inicio');
				}else{
					echo $this->blade->view()->make('page/formulario/login');
				}
			}else{
				echo $this->blade->view()->make('page/formulario/login');
			}
		}
    }
}