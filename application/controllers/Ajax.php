<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends MY_Controller {

    public function template_navigation(){
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $data['data'] = $this->input->post('data');
            echo $this->blade->view()->make("include/navegacion", $data);
        }else {
            redirect('/inicio');
        }
    }

    public function template_proforma(){
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            $data['modelo'] = $this->input->post('modelo');
            $data['pedido'] = $this->input->post('pedido');
            $data['peticion'] = $this->input->post('peticion');
            $this->load->model('modelos');
            $data = $this->modelos->proforma($data);
            echo $this->blade->view()->make("include/cart");
        }else {
            redirect('/inicio');
        }
    }

    public function template_pedido(){
        if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) &&
        strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){

            $data = array();
            if ($this->input->post('tipo')==0){
                $data['inicio'] = $this->input->post('inicio');
            }else{
                $data['inicio'] = $this->input->post('inicio');
                $data['fin'] = $this->input->post('fin');
            }
            $this->load->model('modelos');
            $this->modelos->pedido($data);
        }else {
            redirect('/inicio');
        }
    }
}