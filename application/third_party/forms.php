<?php

class Forms{

    private $frw;

    public function __construct(){
        $this->frw =& get_instance();
        $this->frw->load->helper(array('form'));
        $this->frw->load->database();
    }

    public function perfil($FILES=null){
        if (isset($FILES['img'])){
            if (empty($FILES['img'])){
                $GLOBALS['image_size'] = 90000;
                $GLOBALS['image_width'] = 1024;
                $GLOBALS['image_height'] = 768;
                $GLOBALS['image'] = $FILES['img'];
            }
        }
        $this->frw->form_validation->set_rules('cedula', 'Cedula', array('required', 'integer', 'max_length[10]', 'is_unique[usuario.cedula]', array(
            'option', function($opt){
                if ($opt){
                    $ult_digito=substr($opt, -1,1);//extraigo el ultimo digito de la cedula
                    //extraigo los valores pares//
                    $valor2=substr($opt, 1, 1);
                    $valor4=substr($opt, 3, 1);
                    $valor6=substr($opt, 5, 1);
                    $valor8=substr($opt, 7, 1);
                    $suma_pares=($valor2 + $valor4 + $valor6 + $valor8);
                    //extraigo los valores impares//
                    $valor1=substr($opt, 0, 1);
                    $valor1=($valor1 * 2);
                    if($valor1>9){ 
                        $valor1=($valor1 - 9); 
                    }
                    $valor3=substr($opt, 2, 1);
                    $valor3=($valor3 * 2);
                    if($valor3>9){ 
                        $valor3=($valor3 - 9); 
                    }
                    $valor5=substr($opt, 4, 1);
                    $valor5=($valor5 * 2);
                    if($valor5>9){ 
                        $valor5=($valor5 - 9); 
                    }
                    $valor7=substr($opt, 6, 1);
                    $valor7=($valor7 * 2);
                    if($valor7>9){ 
                        $valor7=($valor7 - 9); 
                    }
                    $valor9=substr($opt, 8, 1);
                    $valor9=($valor9 * 2);
                    if($valor9>9){ 
                        $valor9=($valor9 - 9); 
                    }
                    $suma_impares=($valor1 + $valor3 + $valor5 + $valor7 + $valor9);
                    $suma=($suma_pares + $suma_impares);
                    $dis=substr($suma, 0,1);//extraigo el primer numero de la suma
                    $dis=(($dis + 1)* 10);//luego ese numero lo multiplico x 10, consiguiendo asi la decena inmediata superior
                    $digito=($dis - $suma);
                    if($digito==10){ 
                        $digito='0'; 
                    }
                    if ($digito==$ult_digito){//comparo los digitos final y ultimo
                        return TRUE;
                    }
                }
                $this->frw->form_validation->set_message('option', 'valor de {field} incorrecto');
                return FALSE;
            }
        )));
        $this->frw->form_validation->set_rules('apellidos', 'Apellidos', 'required|max_length[180]');
        $this->frw->form_validation->set_rules('nombres', 'Nombres', 'required|max_length[180]');
        $this->frw->form_validation->set_rules('direccion', 'Direccion', 'required');
        $this->frw->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[200]|is_unique[usuario.email]');
        $this->frw->form_validation->set_rules('genero', 'Genero', 'required');
        $this->frw->form_validation->set_rules('telefono', 'Telefono', 'required|integer|max_length[13]');
        $this->frw->form_validation->set_rules('edad', 'Edad', 'required|integer');
        $this->frw->form_validation->set_rules('username', 'Usuario', 'required|is_unique[usuario.username]');
        $this->frw->form_validation->set_rules('password', 'Contraseña', 'required|max_length[20]');
        $this->frw->form_validation->set_rules('password2', 'Confirmacion Contraseña', 'required|matches[password]');
        if (isset($FILES['img'])){
            if (empty($FILES['img'])){
                $this->frw->form_validation->set_rules('img', 'Imagen', array('trim', array(
                    'option', function($opt){
                        $image = getimagesize($GLOBALS['image']['tmp_name']);
                        if ($image[0] > $GLOBALS['image_width']){
                            $this->frw->form_validation->set_message('option', 'Ancho de {field} excedido');
                            return FALSE;
                        }else if ($image[1] > $GLOBALS['image_height']){
                            $this->frw->form_validation->set_message('option', 'Alto de {field} excedido');
                            return FALSE;
                        }else if ($GLOBALS['image']['size'] > $GLOBALS['image_size']){
                            $this->frw->form_validation->set_message('option', 'Peso de {field} excedido');
                            return FALSE;
                        }
                        return TRUE;
                    }
                )));
            }
        }

        $data = array();

        if ($this->frw->form_validation->run()){
            $img = '';
            $file = 'avatar/';
            if (isset($FILES['img']['name'])){
                $config = array(
                    'upload_path'   => FCPATH.'/uploads/'.$file,
                    'allowed_types' => "jpg|png|jpeg",
                    'overwrite'     => false,
                    'max_size' => (isset($GLOBALS['image_size'])) ? $GLOBALS['image_size'] : null ,
                    'max_width' => (isset($GLOBALS['image_width'])) ? $GLOBALS['image_width'] : null ,
                    'max_height' => (isset($GLOBALS['image_height'])) ? $GLOBALS['image_height'] : null 
                );
                $this->frw->load->library('upload', $config);
                if ($this->frw->upload->do_upload('img')){
                    $img = "{$file}{$this->frw->upload->data('file_name')}";
                }
            }
            $data = array(
                'cedula'     => $this->frw->input->post('cedula'),
                'apellidos'  => $this->frw->input->post('apellidos'),
                'nombres'    => $this->frw->input->post('nombres'),
                'direccion'  => $this->frw->input->post('direccion'),
                'email'      => $this->frw->input->post('email'),
                'genero'     => $this->frw->input->post('genero'),
                'telefono'   => $this->frw->input->post('telefono'),
                'edad'       => $this->frw->input->post('edad'),
                'img'        => $img,
                'username'   => $this->frw->input->post('username'),
                'password'   => $this->frw->input->post('password'),
            );
        }

        return $data;
    }

    public function perfil_modificar($FILES=null, $instance){
        if (isset($FILES['img'])){
            if (empty($FILES['img'])){
                $GLOBALS['image_size'] = 90000;
                $GLOBALS['image_width'] = 1024;
                $GLOBALS['image_height'] = 768;
                $GLOBALS['image'] = $FILES['img'];
            }
        }
        $this->frw->form_validation->set_rules('nombres', 'Nombres', 'required|max_length[180]');
        $this->frw->form_validation->set_rules('apellidos', 'Apellidos', 'required|max_length[180]');
        if ($this->frw->input->post('email')!=$instance->email){
            $this->frw->form_validation->set_rules('email', 'Email', 'trim|required|valid_email|max_length[200]|is_unique[usuario.email]');
        }
        $this->frw->form_validation->set_rules('telefono', 'Telefono', 'required|integer|max_length[13]');
        $this->frw->form_validation->set_rules('edad', 'Edad', 'required|integer');
        $this->frw->form_validation->set_rules('direccion', 'Direccion', 'required');
        if ($this->frw->input->post('password')){
            $this->frw->form_validation->set_rules('password', 'Contraseña', 'required|max_length[20]');
            $this->frw->form_validation->set_rules('password2', 'Confirmacion Contraseña', 'required|matches[password]');
            $instance->password = $this->frw->input->post('password');
        }
        if (isset($FILES['img'])){
            if (empty($FILES['img'])){
                $this->frw->form_validation->set_rules('img', 'Imagen', array('trim', array(
                    'option', function($opt){
                        $image = getimagesize($GLOBALS['image']['tmp_name']);
                        if ($image[0] > $GLOBALS['image_width']){
                            $this->frw->form_validation->set_message('option', 'Ancho de {field} excedido');
                            return FALSE;
                        }else if ($image[1] > $GLOBALS['image_height']){
                            $this->frw->form_validation->set_message('option', 'Alto de {field} excedido');
                            return FALSE;
                        }else if ($GLOBALS['image']['size'] > $GLOBALS['image_size']){
                            $this->frw->form_validation->set_message('option', 'Peso de {field} excedido');
                            return FALSE;
                        }
                        return TRUE;
                    }
                )));
            }
        }

        $data = array();

        if ($this->frw->form_validation->run()){
            $img = '';
            $file = 'avatar/';
            if (isset($FILES['img']['name'])){
                $config = array(
                    'upload_path'   => FCPATH.'/uploads/'.$file,
                    'allowed_types' => "jpg|png|jpeg",
                    'overwrite'     => false,
                    'max_size' => (isset($GLOBALS['image_size'])) ? $GLOBALS['image_size'] : null ,
                    'max_width' => (isset($GLOBALS['image_width'])) ? $GLOBALS['image_width'] : null ,
                    'max_height' => (isset($GLOBALS['image_height'])) ? $GLOBALS['image_height'] : null 
                );
                $this->frw->load->library('upload', $config);
                if ($this->frw->upload->do_upload('img')){
                    $img = "{$file}{$this->frw->upload->data('file_name')}";
                }
            }
            $instance->nombres = $this->frw->input->post('nombres', TRUE);
            $instance->apellidos = $this->frw->input->post('apellidos', TRUE);
            $instance->direccion = $this->frw->input->post('direccion', TRUE);
            $instance->email = $this->frw->input->post('email', TRUE);
            $instance->telefono = $this->frw->input->post('telefono', TRUE);
            $instance->edad = $this->frw->input->post('edad', TRUE);
            $instance->img = $img;
            $instance->password = $this->frw->input->post('password', TRUE);
            if ($this->frw->input->post('email')!=$instance->email){
                $instance->email = $this->frw->input->post('email', TRUE);
            }
            $instance->save();
        }
    }

    public function categoria(){
        $this->frw->form_validation->set_rules('nombre', 'Nombre', 'required|max_length[180]|is_unique[categoria_producto.nombre]');
        $data = array();
        if ($this->frw->form_validation->run()){
            $data = array(
                'nombre' => $this->frw->input->post('nombre')
            );
        }
        return $data;
    }

    public function categoria_modificar($instance){
        if ($instance->nombre != $this->frw->input->post('nombre')){
            $this->frw->form_validation->set_rules('nombre', 'Nombre', 'required|max_length[180]|is_unique[categoria_producto.nombre]');
        }
        $data = array();
        if ($this->frw->form_validation->run()){
            $data = array(
                'nombre' => $this->frw->input->post('nombre')
            );
        }
        return $data;
    }

    public function producto($FILES=null){
        if (isset($FILES['img'])){
            if (empty($FILES['img'])){
                $GLOBALS['image_size'] = 90000;
                $GLOBALS['image_width'] = 1024;
                $GLOBALS['image_height'] = 768;
                $GLOBALS['image'] = $FILES['img'];
            }
        }
        $this->frw->form_validation->set_rules('costo', 'Costo', 'trim|required|max_length[10]|regex_match[/^[0-9]+(\.[0-9]{1,2})?$/]');
        $this->frw->form_validation->set_rules('descripcion', 'Descripción', 'trim|required|max_length[300]');
        $this->frw->form_validation->set_rules('nombre', 'Nombre', 'trim|required|is_unique[producto.nombre]');
        $this->frw->form_validation->set_rules('stock', 'Stock', 'trim|required|integer');
        $this->frw->form_validation->set_rules('categoria', 'categoria', array('trim', 'required', array(
            'option', function($opt){
                if(Categoria_productos::find($opt)){
                    return TRUE;
                }
                $this->frw->form_validation->set_message('option', 'Ingrese {field} valida');
                return FALSE;
            })
        ));
        if (isset($FILES['img'])){
            if (empty($FILES['img'])){
                $this->frw->form_validation->set_rules('img', 'Imagen', array('trim', array(
                    'option', function($opt){
                        $image = getimagesize($GLOBALS['image']['tmp_name']);
                        if ($image[0] > $GLOBALS['image_width']){
                            $this->frw->form_validation->set_message('option', 'Ancho de {field} excedido');
                            return FALSE;
                        }else if ($image[1] > $GLOBALS['image_height']){
                            $this->frw->form_validation->set_message('option', 'Alto de {field} excedido');
                            return FALSE;
                        }else if ($GLOBALS['image']['size'] > $GLOBALS['image_size']){
                            $this->frw->form_validation->set_message('option', 'Peso de {field} excedido');
                            return FALSE;
                        }
                        return TRUE;
                    }
                )));
            }
        }
        $data = array();
        if ($this->frw->form_validation->run()){
            $img = '';
            $file = 'productos/';
            if (isset($FILES['img']['name'])){
                $config = array(
                    'upload_path'   => FCPATH.'/uploads/'.$file,
                    'allowed_types' => "jpg|png|jpeg",
                    'overwrite' => false,
                    'max_size' => (isset($GLOBALS['image_size'])) ? $GLOBALS['image_size'] : null ,
                    'max_width' => (isset($GLOBALS['image_width'])) ? $GLOBALS['image_width'] : null ,
                    'max_height' => (isset($GLOBALS['image_height'])) ? $GLOBALS['image_height'] : null 
                );
                $this->frw->load->library('upload', $config);
                if ($this->frw->upload->do_upload('img')){
                    $img = "{$file}{$this->frw->upload->data('file_name')}";
                }
            }
            $data = array(
                'costo' => $this->frw->input->post('costo'),
                'descripcion' => $this->frw->input->post('descripcion'),
                'nombre' => $this->frw->input->post('nombre'),
                'img' => $img,
                'categoria_id' => $this->frw->input->post('categoria'),
                'stock' => $this->frw->input->post('stock'),
            );
        }
        return $data;
    }

    public function producto_modificar($FILES=null, $instance){
        if (isset($FILES['img'])){
            if (empty($FILES['img'])){
                $GLOBALS['image_size'] = 90000;
                $GLOBALS['image_width'] = 1024;
                $GLOBALS['image_height'] = 768;
                $GLOBALS['image'] = $FILES['img'];
            }
        }
        $nombre = null;
        if ($this->frw->input->post('nombre')!=$instance->nombre){
            $this->frw->form_validation->set_rules('nombre', 'Nombre', 'trim|required|max_length[40]|is_unique[producto.nombre]');
            $nombre = $this->frw->input->post('nombre');
        }
        $this->frw->form_validation->set_rules('costo', 'Costo', 'trim|required|max_length[10]|regex_match[/^[0-9]+(\.[0-9]{1,2})?$/]');
        $this->frw->form_validation->set_rules('stock', 'Stock', 'trim|required|integer');
        $this->frw->form_validation->set_rules('descripcion', 'Descripción', 'trim|required');
        $this->frw->form_validation->set_rules('categoria', 'categoria', array('trim', 'required', array(
            'option', function($opt){
                if(Categoria_productos::find($opt)){
                    return TRUE;
                }
                $this->frw->form_validation->set_message('option', 'Ingrese {field} valida');
                return FALSE;
            })
        ));
        if (isset($FILES['img'])){
            if (empty($FILES['img'])){
                $this->frw->form_validation->set_rules('img', 'Imagen', array('trim', array(
                    'option', function($opt){
                        $image = getimagesize($GLOBALS['image']['tmp_name']);
                        if ($image[0] > $GLOBALS['image_width']){
                            $this->frw->form_validation->set_message('option', 'Ancho de {field} excedido');
                            return FALSE;
                        }else if ($image[1] > $GLOBALS['image_height']){
                            $this->frw->form_validation->set_message('option', 'Alto de {field} excedido');
                            return FALSE;
                        }else if ($GLOBALS['image']['size'] > $GLOBALS['image_size']){
                            $this->frw->form_validation->set_message('option', 'Peso de {field} excedido');
                            return FALSE;
                        }
                        return TRUE;
                    }
                )));
            }
        }
        $data = array();
        if ($this->frw->form_validation->run()){
            $img = '';
            $file = 'productos/';
            if (isset($FILES['img']['name'])){
                $config = array(
                    'upload_path'   => FCPATH.'/uploads/'.$file,
                    'allowed_types' => "jpg|png|jpeg",
                    'overwrite' => false,
                    'max_size' => (isset($GLOBALS['image_size'])) ? $GLOBALS['image_size'] : null ,
                    'max_width' => (isset($GLOBALS['image_width'])) ? $GLOBALS['image_width'] : null ,
                    'max_height' => (isset($GLOBALS['image_height'])) ? $GLOBALS['image_height'] : null 
                );
                $this->frw->load->library('upload', $config);
                if ($this->frw->upload->do_upload('img')){
                    $img = "{$file}{$this->frw->upload->data('file_name')}";
                }
            }
            $data = array(
                'costo' => $this->frw->input->post('costo', TRUE),
                'stock' => $this->frw->input->post('stock', TRUE),
                'nombre' => $nombre,
                'descripcion' => $this->frw->input->post('descripcion', TRUE),
                'categoria_id' => $this->frw->input->post('categoria', TRUE),
                'img' => $img,
            );
        }
        return $data;
    }

    public function servicio($FILES=null){
        if (isset($FILES['img'])){
            if (empty($FILES['img'])){
                $GLOBALS['image_size'] = 90000;
                $GLOBALS['image_width'] = 1024;
                $GLOBALS['image_height'] = 768;
                $GLOBALS['image'] = $FILES['img'];
            }
        }
        $this->frw->form_validation->set_rules('costo', 'Costo', 'trim|required|max_length[10]|regex_match[/^[0-9]+(\.[0-9]{1,2})?$/]');
        $this->frw->form_validation->set_rules('descripcion', 'Descripción', 'trim|required|max_length[300]');
        $this->frw->form_validation->set_rules('nombre', 'Nombre', 'trim|required|is_unique[producto.nombre]');
        $this->frw->form_validation->set_rules('categoria', 'categoria', array('trim', 'required', array(
            'option', function($opt){
                if(Categoria_servicios::find($opt)){
                    return TRUE;
                }
                $this->frw->form_validation->set_message('option', 'Ingrese {field} valida');
                return FALSE;
            })
        ));
        if (isset($FILES['img'])){
            if (empty($FILES['img'])){
                $this->frw->form_validation->set_rules('img', 'Imagen', array('trim', array(
                    'option', function($opt){
                        $image = getimagesize($GLOBALS['image']['tmp_name']);
                        if ($image[0] > $GLOBALS['image_width']){
                            $this->frw->form_validation->set_message('option', 'Ancho de {field} excedido');
                            return FALSE;
                        }else if ($image[1] > $GLOBALS['image_height']){
                            $this->frw->form_validation->set_message('option', 'Alto de {field} excedido');
                            return FALSE;
                        }else if ($GLOBALS['image']['size'] > $GLOBALS['image_size']){
                            $this->frw->form_validation->set_message('option', 'Peso de {field} excedido');
                            return FALSE;
                        }
                        return TRUE;
                    }
                )));
            }
        }
        $data = array();
        if ($this->frw->form_validation->run()){
            $img = '';
            $file = 'productos/';
            if (isset($FILES['img']['name'])){
                $config = array(
                    'upload_path'   => FCPATH.'/uploads/'.$file,
                    'allowed_types' => "jpg|png|jpeg",
                    'overwrite' => false,
                    'max_size' => (isset($GLOBALS['image_size'])) ? $GLOBALS['image_size'] : null ,
                    'max_width' => (isset($GLOBALS['image_width'])) ? $GLOBALS['image_width'] : null ,
                    'max_height' => (isset($GLOBALS['image_height'])) ? $GLOBALS['image_height'] : null 
                );
                $this->frw->load->library('upload', $config);
                if ($this->frw->upload->do_upload('img')){
                    $img = "{$file}{$this->frw->upload->data('file_name')}";
                }
            }
            $data = array(
                'costo' => $this->frw->input->post('costo'),
                'descripcion' => $this->frw->input->post('descripcion'),
                'nombre' => $this->frw->input->post('nombre'),
                'img' => $img,
                'categoria_id' => $this->frw->input->post('categoria'),
            );
        }
        return $data;
    }

    public function servicio_modificar($FILES=null, $instance){
        if (isset($FILES['img'])){
            if (empty($FILES['img'])){
                $GLOBALS['image_size'] = 90000;
                $GLOBALS['image_width'] = 1024;
                $GLOBALS['image_height'] = 768;
                $GLOBALS['image'] = $FILES['img'];
            }
        }
        $nombre = null;
        if ($this->frw->input->post('nombre')!=$instance->nombre){
            $this->frw->form_validation->set_rules('nombre', 'Nombre', 'trim|required|max_length[40]|is_unique[producto.nombre]');
            $nombre = $this->frw->input->post('nombre');
        }
        $this->frw->form_validation->set_rules('costo', 'Costo', 'trim|required|max_length[10]|regex_match[/^[0-9]+(\.[0-9]{1,2})?$/]');
        $this->frw->form_validation->set_rules('descripcion', 'Descripción', 'trim|required');
        $this->frw->form_validation->set_rules('categoria', 'categoria', array('trim', 'required', array(
            'option', function($opt){
                if(Categoria_servicios::find($opt)){
                    return TRUE;
                }
                $this->frw->form_validation->set_message('option', 'Ingrese {field} valida');
                return FALSE;
            })
        ));
        if (isset($FILES['img'])){
            if (empty($FILES['img'])){
                $this->frw->form_validation->set_rules('img', 'Imagen', array('trim', array(
                    'option', function($opt){
                        $image = getimagesize($GLOBALS['image']['tmp_name']);
                        if ($image[0] > $GLOBALS['image_width']){
                            $this->frw->form_validation->set_message('option', 'Ancho de {field} excedido');
                            return FALSE;
                        }else if ($image[1] > $GLOBALS['image_height']){
                            $this->frw->form_validation->set_message('option', 'Alto de {field} excedido');
                            return FALSE;
                        }else if ($GLOBALS['image']['size'] > $GLOBALS['image_size']){
                            $this->frw->form_validation->set_message('option', 'Peso de {field} excedido');
                            return FALSE;
                        }
                        return TRUE;
                    }
                )));
            }
        }
        $data = array();
        if ($this->frw->form_validation->run()){
            $img = '';
            $file = 'productos/';
            if (isset($FILES['img']['name'])){
                $config = array(
                    'upload_path'   => FCPATH.'/uploads/'.$file,
                    'allowed_types' => "jpg|png|jpeg",
                    'overwrite' => false,
                    'max_size' => (isset($GLOBALS['image_size'])) ? $GLOBALS['image_size'] : null ,
                    'max_width' => (isset($GLOBALS['image_width'])) ? $GLOBALS['image_width'] : null ,
                    'max_height' => (isset($GLOBALS['image_height'])) ? $GLOBALS['image_height'] : null 
                );
                $this->frw->load->library('upload', $config);
                if ($this->frw->upload->do_upload('img')){
                    $img = "{$file}{$this->frw->upload->data('file_name')}";
                }
            }
            $data = array(
                'costo' => $this->frw->input->post('costo', TRUE),
                'nombre' => $nombre,
                'descripcion' => $this->frw->input->post('descripcion', TRUE),
                'categoria_id' => $this->frw->input->post('categoria', TRUE),
                'img' => $img
            );
        }
        return $data;
    }
}