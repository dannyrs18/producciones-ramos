<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Pedidos extends Eloquent {

    protected $table = "pedido"; // table name

    public function proformas(){
        return Proformas::all()->where('pedido_id', $this->id);
    }
    
    public function cliente(){
        return Usuarios::find($this->cliente_id);
    }
}

# excecute composer dump-autoload