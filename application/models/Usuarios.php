<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Usuarios extends Eloquent {

    protected $table = "usuario"; // table name

    public function get_simple_name(){
        return explode(' ', $this->nombres)[0].' '.explode(' ', $this->apellidos)[0];
    }

    public function get_full_name(){
        return "{$this->nombres} {$this->apellidos}";
    }
}

# excecute composer dump-autoload