<?php

class Modelos extends CI_Model {

    public function usuario($data){
        $usuario = new Usuarios;
        $usuario->cedula = $data['cedula'];
        $usuario->apellidos = $data['apellidos'];
        $usuario->nombres = $data['nombres'];
        $usuario->direccion = $data['direccion'];
        $usuario->email = $data['email'];
        $usuario->genero = $data['genero'];
        $usuario->telefono = $data['telefono'];
        $usuario->edad = $data['edad'];
        $usuario->estado = '1';
        $usuario->img = $data['img'];
        $usuario->username = $data['username'];
        $usuario->password = password_hash($data['password'], PASSWORD_DEFAULT);
        $usuario->rol = $data['rol'];
        $usuario->slug = substr(md5(mt_rand()), 0, 40);
        $usuario->save();
    }

    public function categoria_producto(array $data){
        $categoria = new Categoria_productos;
        $categoria->nombre = $data['nombre'];
        $categoria->slug = substr(md5(mt_rand()), 0, 40);
        $categoria->save();
    }

    public function producto(array $data){
        $producto = new Productos;
        $producto->costo = $data['costo'];
        $producto->descripcion = $data['descripcion'];
        $producto->nombre = $data['nombre'];
        $producto->img = $data['img'];
        $producto->categoria_id = $data['categoria_id'];
        $producto->stock = $data['stock'];
        $producto->disponibilidad = $data['stock'];
        $producto->slug = substr(md5(mt_rand()), 0, 40);
        $producto->save();
    }

    public function categoria_servicio(array $data){
        $categoria = new Categoria_servicios;
        $categoria->nombre = $data['nombre'];
        $categoria->slug = substr(md5(mt_rand()), 0, 40);
        $categoria->save();
    }

    public function servicio(array $data){
        $servicio = new Servicios;
        $servicio->costo = $data['costo'];
        $servicio->descripcion = $data['descripcion'];
        $servicio->nombre = $data['nombre'];
        $servicio->img = $data['img'];
        $servicio->categoria_id = $data['categoria_id'];
        $servicio->slug = substr(md5(mt_rand()), 0, 40);
        $servicio->save();
    }

    public function pedido($data){
        $pedido = new Pedidos;
        $pedido->num_pedido = substr(md5(mt_rand()), 0, 15);
        $pedido->cliente_id = $this->session->id;
        $date = DateTime::createFromFormat('d/m/Y', $data['inicio']);
        $pedido->fecha_inicio = $date->format('Y-m-d');;
        if (isset($data['fin'])){
            $date = DateTime::createFromFormat('d/m/Y', $data['fin']);
            $pedido->fecha_fin = $date->format('Y-m-d');;
        }
        $pedido->save();
    }

    public function proforma($data){
        $pedido = Pedidos::where('estado', '1')->where('cliente_id', $this->session->id)->first();
        if ($data['modelo']=='producto') {
            $instance = Proformas::where('producto_id', $data['pedido'])->where('pedido_id', $pedido->id)->first();
            $producto = Productos::find($data['pedido']);
            if ($pedido==TRUE & $instance==TRUE){
                if ($data['peticion']=='agregar'){
                    $instance->costo = $instance->costo + $producto->costo;
                    $instance->cantidad = $instance->cantidad + 1;
                    $instance->save();
                    $producto->disponibilidad=$producto->disponibilidad - 1;
                    $producto->save();
                }else if($data['peticion']=='eliminar'){
                    $instance->costo = $instance->costo - $producto->costo;
                    $instance->cantidad = $instance->cantidad - 1;
                    if($instance->cantidad != 0){
                        $instance->save();
                    }else{
                        $instance->delete();
                    }
                    $producto->disponibilidad=$producto->disponibilidad + 1;
                    $producto->save();
                }
            }else{
                $proforma = new Proformas;
                $proforma->producto_id = $producto->id;
                $proforma->pedido_id = $pedido->id;
                $proforma->costo = $producto->costo;
                $proforma->save();
                $producto->disponibilidad=$producto->disponibilidad - 1;
                $producto->save();
            }
        } else if ($data['modelo']=='servicio'){
            $servicio = Servicios::find($data['pedido']);
            if ($data['peticion']=='agregar'){
                $proforma = new Proformas;
                $proforma->servicio_id = $servicio->id;
                $proforma->pedido_id = $pedido->id;
                $proforma->costo = $servicio->costo;
                $proforma->save();
            }else if($data['peticion']=='eliminar'){
                $instance = Proformas::where('servicio_id', $data['pedido'])->where('pedido_id', $pedido->id)->first();
                $instance->delete();
            }
        }
    }

}