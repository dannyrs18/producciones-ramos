<?php 

use \Illuminate\Database\Eloquent\Model as Eloquent;

class Proformas extends Eloquent {

    protected $table = "proforma"; // table name

    public function producto(){
        if ($this->producto_id) {
            return Productos::find($this->producto_id);
        }
        return array();
    }
    public function servicio(){
        if ($this->servicio_id) {
            return Servicios::find($this->servicio_id);
        }
        return array();
    }

}

# excecute composer dump-autoload