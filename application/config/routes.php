<?php
defined('BASEPATH') OR exit('No direct script access allowed');

$route['default_controller'] = 'page/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

# Paginas de informacion
$route['(:any)'] = 'page/index/$1';

# Pagina de registro
$route['page/registro'] = 'page/registro';

# Pagina de login
$route['page/login'] = 'page/login';

# Pagina de inicio del administrador
$route['admin/inicio'] = 'admin/page/inicio';
$route['admin/respaldo'] = 'admin/page/respaldo';

# Migraciones
$route['migrate/(:num)'] = 'migrate/index/$1';

# Usuario
$route['admin/usuario/tabla'] = 'admin/usuario/obtener/usuario';
$route['admin/usuario/estado/(:any)'] = 'admin/usuario/estado/usuario/$1';

# Cliente
$route['admin/cliente/tabla'] = 'admin/usuario/obtener/cliente';
$route['admin/cliente/estado/(:any)'] = 'admin/usuario/estado/cliente/$1';

# Categoria producto
$route['admin/categoria/producto/crear'] = 'admin/categoria_producto/crear';
$route['admin/categoria/producto/tabla'] = 'admin/categoria_producto/obtener';
$route['admin/categoria/producto/modificar/(:any)'] = 'admin/categoria_producto/modificar/$1';
$route['admin/categoria/producto/estado/(:any)'] = 'admin/categoria_producto/estado/$1';

# Categoria servicio
$route['admin/categoria/servicio/crear'] = 'admin/categoria_servicio/crear';
$route['admin/categoria/servicio/tabla'] = 'admin/categoria_servicio/obtener';
$route['admin/categoria/servicio/modificar/(:any)'] = 'admin/categoria_servicio/modificar/$1';
$route['admin/categoria/servicio/estado/(:any)'] = 'admin/categoria_servicio/estado/$1';

# Producto
$route['admin/producto/tabla'] = 'admin/producto/obtener';

# Servicio
$route['admin/servicio/tabla'] = 'admin/servicio/obtener';

# Cerrar sesion
$route['admin/logout'] = 'admin/page/logout';
