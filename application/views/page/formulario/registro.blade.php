<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<title>Producciones Ramos</title>
<!-- Tell the browser to be responsive to screen width -->
<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet" href="<?= base_url() ?>assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet" href="<?= base_url() ?>assets/admin/bower_components/font-awesome/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet" href="<?= base_url() ?>assets/admin/bower_components/Ionicons/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet" href="<?= base_url() ?>assets/admin/dist/css/AdminLTE.min.css">
<!-- iCheck -->
<link rel="stylesheet" href="<?= base_url() ?>assets/admin/plugins/iCheck/square/blue.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->

<!-- Google Font -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition register-page" style="background: url('{{ base_url() }}assets/page/images/back.jpg') no-repeat center center fixed; background-size: cover">
<div class="register-box">  
    <div class="login-logo">
        <a href="/inicio"><b>Producciones</b>Ramos</a>
    </div>

    <div class="register-box-body">
        <p class="login-box-msg"><strong>REGISTRO DE CLIENTES</strong></p>

        <?php echo validation_errors(); ?>

        <?php echo form_open_multipart(''); ?> 
        <div class="form-group has-feedback">
            <input type="number" class="form-control" placeholder="Cedula" name="cedula" value="{{ set_value('cedula') }}" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Nombres" name="nombres" value="{{ set_value('nombres') }}" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Apellidos" name="apellidos" value="{{ set_value('apellidos') }}" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <textarea class="form-control" placeholder="Direccion" name="direccion" rows="2">{{ set_value('direccion') }}</textarea>
        </div>
        <div class="form-group has-feedback">
            <input type="email" class="form-control" placeholder="Email" name="email" value="{{ set_value('email') }}" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="number" class="form-control" placeholder="Telefono" name="telefono" value="{{ set_value('telefono') }}" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="number" class="form-control" placeholder="Edad" name="edad" value="{{ set_value('edad') }}" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <label>Genero</label>
            <br>
            <input type="radio" name="genero" value="1" class="form-control">Hombre
            <br>
            <input type="radio" name="genero" value="0" class="form-control">Mujer
        </div>
        <div class="form-group has-feedback">
            <input type="text" class="form-control" placeholder="Usuario" name="username" value="{{ set_value('username') }}" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Contraseña" name="password" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <input type="password" class="form-control" placeholder="Repita la contraseña" name="password2" required>
            <span class="glyphicon glyphicon-user form-control-feedback"></span>
        </div>
        <div class="form-group has-feedback">
            <label for="img">Foto de perfil</label>
            <input type="file" class="form-control" name="img" id="img">
        </div>
        <div class="row">
            <div class="col-xs-8">
                <div class="checkbox icheck"></div>
            </div>
            <!-- /.col -->
            <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Register</button>
            </div>
            <!-- /.col -->
        </div>
        </form>
    </div>
    <!-- /.form-box -->
</div>
<!-- /.register-box -->

<!-- jQuery 3 -->
<script src="<?= base_url() ?>assets/admin/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="<?= base_url() ?>assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- iCheck -->
<script src="<?= base_url() ?>assets/admin/plugins/iCheck/icheck.min.js"></script>
<script>
$(function () {
    $('input').iCheck({
    checkboxClass: 'icheckbox_square-blue',
    radioClass: 'iradio_square-blue',
    increaseArea: '20%' /* optional */
    });
});
</script>
</body>
</html>
