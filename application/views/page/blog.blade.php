@extends('template/base')

@section('content')
<div class="col-md-4 address-more">
            <h4>Contactanos</h4>
            <div class="address-grid">
                <i class="glyphicon glyphicon-globe"></i>
                <div class="address1">
                    <p>Contactanos</p>
                    <p>TL 2683 567</p>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="address-grid grid-address">
                <i class="glyphicon glyphicon-phone"></i>
                <div class="address1">
                    <p>+593959576573</p>
                </div>
                <div class="clearfix"> </div>
            </div>
            </div>
                <div class="col-md-8 map">
                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d995.0660831084737!2d-79.19646873648708!3d-3.97223070487829!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1ses-419!2sus!4v1541563909379" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>                        
            </div>
            <div class="clearfix"> </div>
            </div>
        </div>
    </div>
@endsection