@extends('template/base')

@section('content')
<div class="content-middle">
		<div class="container">
		<div class="content-mid-top">
					<h3> Acerca de</h3>
				</div>
				<div class="news">
					<div class="col-md-6 new-more">
						<div class=" new-more1">
						<div class="col-md-2 six">						
							<img class="img-responsive" src="images/te.jpg" alt="">
						</div>
						<div class="col-md-10 six1">
							<h5>Marco Institucional</h5>
							<p>La empresa Producciones Ramos fue creada el 20 de mayo de 2013 ubicada en el sector la Samana en la ciudad de Loja, brinda a la comunidad lojana los servicios de alquiler de sonido y luces.</p>
						<a href="single.html"><i class=""></i></a>
						</div>
						<div class="clearfix"> </div>
					</div>
					</div>
						<div class="col-md-6 new-more">
						<div class=" new-more1">
						<div class="col-md-2 six">
							<img class="img-responsive" src="images/te1.jpg" alt="">
						</div>
						<div class="col-md-10 six1">
							<h5>Misión</h5>
							<p>Consolidarse dentro del mercado como una empresa de eventos y servicio que ofrece excelente calidad, alto compromiso con los clientes, cumplimiento óptimo y eficiente de las tareas, extraordinario y honesto equipo de trabajo y principalmente ser una empresa amigable con el medio ambiente, adoptando prácticas ecológicas y sustentables.</p>
							<a href="single.html"><i class=""></i></a>						</div>
						<div class="clearfix"> </div>
					</div>
					</div>
					<div class="clearfix"> </div>
				</div>
				<div class="news">
					<div class="col-md-6 new-more">
						<div class=" new-more1">
						<div class="col-md-2 six">
							<img class="img-responsive" src="images/te3.jpg" alt="">
						</div>
						<div class="col-md-10 six1">
							<h5>Visión</h5>
							<p>Promover la confianza de nuestros clientes para nuestro servicio, innovando, desarrollando y procurando reafirmar el prestigio de nuestra empresa con la afinidad de que la Empresa Producciones Ramos cubre el mercado del sonido y los servicios complementarios.</p>
							<a href="single.html"><i class=""></i></a>						</div>
						<div class="clearfix"> </div>
					</div>
					</div>
					<div class="clearfix"> </div>
				</div>
			</div>
	</div>
@endsection