<!--A Design by W3layouts 
Author: W3layout
Author URL: http://w3layouts.com
License: Creative Commons Attribution 3.0 Unported
License URL: http://creativecommons.org/licenses/by/3.0/
-->
<!DOCTYPE html>
<html>
<head>
<title>Producciones Ramos</title>
<link href="<?= base_url() ?>assets/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
<link href="<?php base_url() ?>assets/page/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="<?php base_url() ?>assets/page/js/jquery.min.js"></script>
<!-- Custom Theme files -->
<!--theme-style-->
<link href="<?php base_url() ?>assets/page/css/style.css" rel="stylesheet" type="text/css" media="all" />	
<!--//theme-style-->
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="Music Club Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template, 
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
</head>
<body>
    <!--header-->
    <div class="header">
        <div class="container">
        @include('include/nav')
        <div class="banner-main">
            <section class="slider">
                <div class="flexslider">
                    <ul class="slides">
                        <li>
                            <div class="banner-matter">
                                <h3>Llevamos tus eventos a otro nivel</h3>
                                <p>Lo mejor en sonido</p>
                            </div>
                        </li>
                        <li>
                            <div class="banner-matter">
                                <h3>Lo mejor en iluminación</h3>
                                <p></p>
                            </div>
                        </li>
                        <li>
                            <div class="banner-matter">
                                <h3>Lo mejor en productos</h3>
                                <p></p>
                            </div>
                        </li>
                    </ul>
                </div>
            </section>
        </div>

    <!--FlexSlider-->
        <link rel="stylesheet" href="<?php base_url() ?>assets/page/css/flexslider.css" type="text/css" media="screen" />
        <script defer src="<?php base_url() ?>assets/page/js/jquery.flexslider.js"></script>
        <script type="text/javascript">
        $(function(){
        SyntaxHighlighter.all();
        });
        $(window).load(function(){
        $('.flexslider').flexslider({
            animation: "slide",
            start: function(slider){
            $('body').removeClass('loading');
            }
        });
        });
    </script>

        </div>
    </div>
    <!--//header-->
    <!--content-->
    <div class="content">
        <div class="container">
            <!--content-top-->
            <div class="content-top">
                <div class="col-md-7 content-top1">
                    <h3>Bienvenidos</h3>
                    <p>Producciones Ramos es una empresa lojana que viene laborando a partir del año 2013 brindando servicios garantizados en producción, asesoría y alquiler en sonido e iluminación, cuenta con estudio de grabación ya sea para edición de spot, jingles, cuñas, entre otras. También organiza eventos socio-culturales ofreciendo maestros de ceremonias, servicios de animación, discomóvil, dj’s dentro y fuera de la ciudad.</p>
                </div>
                <div class="col-md-5 top-col">
                    <div class="col1">
                        <div class="col-md-6 col2">
                            <img src="<?php base_url() ?>assets/page/images/ic.png" alt="" >
                        </div>
                        <div class="col-md-6 col3">
                            <img src="<?php base_url() ?>assets/page/images/ic1.png" alt="" >
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                    <div class="col1">
                        <div class="col-md-6 col4">
                            <img src="<?php base_url() ?>assets/page/images/ic2.png" alt="" >
                        </div>
                        <div class="col-md-6 col5">
                            <img src="<?php base_url() ?>assets/page/images/ic3.png" alt="" >
                        </div>
                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
            <!--//content-top-->
            <!--content-mid-->
            <div class="content-mid">
                <div class="col-md-4 mid">
                    <a href="single"><img src="<?php base_url() ?>assets/page/images/i1.jpg" alt="" class="img-responsive">
                    <div class="mid1">
                        <h4>Djs</h4>
                        <i class="glyphicon glyphicon-circle-arrow-right"></i>
                        <div class="clearfix"> </div>
                    </div>
                    </a>
                </div>
                <div class="col-md-4 mid">
                    <a href="single"><img src="<?php base_url() ?>assets/page/images/i2.jpg" alt="" class="img-responsive">
                    <div class="mid1">
                        <h4>Animadores</h4>
                        <i class="glyphicon glyphicon-circle-arrow-right"></i>
                        <div class="clearfix"> </div>
                    </div>
                    </a>
                </div>
                <div class="col-md-4 mid">
                    <a href="single"><img src="<?php base_url() ?>assets/page/images/i3.jpg" alt="" class="img-responsive">
                    <div class="mid1">
                        <h4>Productos</h4>
                        <i class="glyphicon glyphicon-circle-arrow-right"></i>
                        <div class="clearfix"> </div>
                    </div>
                    </a>
                </div>
                <div class="clearfix"> </div>
            </div>
            <!--content-mid-->
        </div>
        
    </div>
    <!--//content-->
    <!--footer-->
    @include('include/footer')
    <!--//footer-->
</body>
</html>