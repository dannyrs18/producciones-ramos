@extends('template/base')

@section('style')
<script src="<?php base_url() ?>assets/page/js/modernizr.custom.97074.js"></script>
<!--script-->
<script src="<?php base_url() ?>assets/page/js/jquery.chocolat.js"></script>
<link rel="stylesheet" href="<?php base_url() ?>assets/page/css/chocolat.css" type="text/css" media="screen" charset="utf-8">
	<!--light-box-files -->
<script type="text/javascript" charset="utf-8">
	$(function() {
		$('.gallery a').Chocolat();
	});
</script>
@endsection

@section('content')
<div class="gallery">
	<div class="container">
		<h3>Galeria de Eventos</h3>
		<section>
			<ul id="da-thumbs" class="da-thumbs">
				<li>
					<a href="<?php base_url() ?>assets/page/images/01.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
						<img src="<?php base_url() ?>assets/page/images/01.jpg" alt="" />
						<div>
							<h5>Registrate</h5>
							<span>Llevamos tus eventos a otro nivel</span>
						</div>
					</a>
				</li>
				<li>
					<a href="<?php base_url() ?>assets/page/images/02.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
						<img src="<?php base_url() ?>assets/page/images/02.jpg" alt="" />
						<div>
							<h5>Registrate</h5>
							<span>Llevamos tus eventos a otro nivel</span>
						</div>
					</a>
				</li>
				<li>
					<a href="<?php base_url() ?>assets/page/images/03.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
						<img src="<?php base_url() ?>assets/page/images/03.jpg" alt="" />
						<div>
							<h5>Registrate</h5>
							<span>Llevamos tus eventos a otro nivel</span>
						</div>
					</a>
				</li>
				<li>
					<a href="<?php base_url() ?>assets/page/images/04.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
						<img src="<?php base_url() ?>assets/page/images/04.jpg" alt="" />
						<div>
							<h5>Registrate</h5>
							<span>Llevamos tus eventos a otro nivel</span>
						</div>
					</a>
				</li>
				<li>	
					<a href="<?php base_url() ?>assets/page/images/05.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
						<img src="<?php base_url() ?>assets/page/images/05.jpg" alt="" />
						<div>
							<h5>Registrate</h5>
							<span>Llevamos tus eventos a otro nivel</span>
						</div>
					</a>
				</li>
				<li>
					<a href="<?php base_url() ?>assets/page/images/06.jpg" rel="title" class="b-link-stripe b-animate-go  thickbox">
						<img src="<?php base_url() ?>assets/page/images/06.jpg" alt="" />
						<div>
							<h5>Registrate</h5>
							<span>Llevamos tus eventos a otro nivel</span>
						</div>
					</a>
				</li>
				<div class="clearfix"> </div>
			</ul>
		</section>
	</div>
</div>
@endsection

@section('script')
<script type="text/javascript" src="<?php base_url() ?>assets/page/js/jquery.hoverdir.js"></script>	
<script type="text/javascript">
	$(function() {
		$(' #da-thumbs > li ').each( function() { $(this).hoverdir(); } );
	});
</script>
@endsection