<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <link href="<?= base_url() ?>assets/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
  <title>Producciones Ramos</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="{{ base_url() }}assets/admin/bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="{{ base_url() }}assets/admin/bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="{{ base_url() }}assets/admin/bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="{{ base_url() }}assets/admin/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
      folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="{{ base_url() }}assets/admin/dist/css/skins/_all-skins.min.css">
  
  <!-- izitoast -->
  <link rel="stylesheet" href="{{ base_url() }}assets/admin/bower_components/izitoast/dist/css/iziToast.min.css">
  <!-- daterange picker -->
  <link rel="stylesheet" href="{{ base_url() }}assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="{{ base_url() }}assets/admin/bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="{{ base_url() }}assets/admin/plugins/timepicker/bootstrap-timepicker.min.css">
  

  @yield('style')

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<!-- ADD THE CLASS fixed TO GET A FIXED HEADER AND SIDEBAR LAYOUT -->
<!-- the fixed layout is not compatible with sidebar-mini -->
<body class="fixed sidebar-mini sidebar-mini-expand-feature skin-black">
  <!-- Site wrapper -->
  <div class="wrapper">

    <header class="main-header">
      <!-- Logo -->
      <a href="/admin/inicio" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>P</b>R</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>PROD </b>Ramos</span>
      </a>
      <!-- Header Navbar: style can be found in header.less -->
      <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </a>
        
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- Notifications: style can be found in dropdown.less -->
            @if (in_array($_SESSION['rol'], array(2)))
            <li class="dropdown notifications-menu" id="fecha_pedido">
              @include('include/pedido')
            </li>
            <li class="dropdown notifications-menu" id="cart">
              @include('include/cart')
            </li>
            @endif
            <!-- User Account: style can be found in dropdown.less -->
            <li class="dropdown user user-menu">
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <img src="{{ base_url() }}assets/admin/dist/img/user.png" class="user-image" alt="User Image">
                <span class="hidden-xs">{{ $_SESSION['full_name'] }}</span>
              </a>
              <ul class="dropdown-menu">
                <!-- User image -->
                <li class="user-header">
                    <img src="{{ base_url() }}assets/admin/dist/img/user.png" class="img-circle" alt="User Image">
                  <p>
                    {{ $_SESSION['full_name'] }}
                    <small>{{ $_SESSION['email'] }}</small>
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="/admin/usuario/modificar/{{ $_SESSION['slug'] }}" class="btn btn-default btn-flat">Configurar</a>
                  </div>
                  <div class="pull-right">
                    <a href="/admin/logout" class="btn btn-default btn-flat">Cerrar Sesión</a>
                  </div>
                </li>
              </ul>
            </li>

          </ul>
        </div>
      </nav>
    </header>
    <!-- =============================================== -->

    <!-- Left side column. contains the sidebar -->
    <aside class="main-sidebar">
      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
          <div class="pull-left image">
            @if ($_SESSION['img'])
            <img src="{{ base_url() }}uploads/{{ $_SESSION['img'] }}" class="img-circle" alt="User Image" style="width: 50px; height: 50px">
            @else
            <img src="{{ base_url() }}assets/admin/dist/img/user.png" class="img-circle" alt="User Image">
            @endif
          </div>
          <div class="pull-left info">
            <p>{{ $_SESSION['simple_name'] }}</p>
            <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          </div>
        </div>
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu" data-widget="tree">
          <li class="header">ADMINISTRACIÓN</li>
          @if (in_array($_SESSION['rol'], array(0)))
          <li class="treeview">
            <a href="#">
              <i class="fa fa-user"></i> <span>USUARIOS</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="/admin/usuario/crear"><i class="fa fa-circle-o"></i> CREAR</a></li>
              <li><a href="/admin/usuario/tabla"><i class="fa fa-circle-o"></i> BUSCAR</a></li>
            </ul>
          </li>
          @endif
  
          @if (in_array($_SESSION['rol'], array(0,1)))
          <li class="treeview">
            <a href="#">
              <i class="fa fa-users"></i> <span>CLIENTES</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="/admin/cliente/tabla"><i class="fa fa-circle-o"></i> BUSCAR</a></li>
            </ul>
          </li>
          @endif

          @if (in_array($_SESSION['rol'], array(0,1)))
          <li class="treeview">
            <a href="#">
              <i class="fa fa-share"></i> <span>CATEGORIAS</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li class="treeview">
                <a href="#"><i class="fa fa-circle-o"></i> PRODUCTOS
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="/admin/categoria/producto/crear"><i class="fa fa-circle-o"></i> NUEVO</a></li>
                  <li><a href="/admin/categoria/producto/tabla"><i class="fa fa-circle-o"></i> BUSCAR</a></li>
                </ul>
              </li>
              <li class="treeview">
                <a href="#"><i class="fa fa-circle-o"></i> SERVICIOS
                  <span class="pull-right-container">
                    <i class="fa fa-angle-left pull-right"></i>
                  </span>
                </a>
                <ul class="treeview-menu">
                  <li><a href="/admin/categoria/servicio/crear"><i class="fa fa-circle-o"></i> NUEVO</a></li>
                  <li><a href="/admin/categoria/servicio/tabla"><i class="fa fa-circle-o"></i> BUSCAR</a></li>
                </ul>
              </li>
            </ul>
          </li>
          @endif
  
          @if (in_array($_SESSION['rol'], array(0,1)))
          <li class="treeview">
            <a href="#">
              <i class="fa fa-cart-plus"></i> <span>PRODUCTOS</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="/admin/producto/crear"><i class="fa fa-circle-o"></i>NUEVO</a></li>
              <li><a href="/admin/producto/tabla"><i class="fa fa-circle-o"></i> BUSCAR</a></li>
              <li><a href="/admin/producto/reporte"><i class="fa fa-circle-o"></i> REPORTE</a></li>
            </ul>
          </li>
          @endif

          @if (in_array($_SESSION['rol'], array(0,1)))
          <li class="treeview">
            <a href="#">
              <i class="fa fa-server"></i> <span>SERVICIOS</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              <li><a href="/admin/servicio/crear"><i class="fa fa-circle-o"></i>NUEVO</a></li>
              <li><a href="/admin/servicio/tabla"><i class="fa fa-circle-o"></i> BUSCAR</a></li>
              <li><a href="/admin/servicio/reporte"><i class="fa fa-circle-o"></i> REPORTE</a></li>
            </ul>
          </li>
          @endif

          @if (in_array($_SESSION['rol'], array(2)))
          <li class="treeview">
            <a href="#">
              <i class="fa fa-calendar"></i> <span>SERVICIOS</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              @foreach (Categoria_servicios::all()->where('estado', '1') as $item)
              <li><a href="/admin/reservacion/servicios/{{ $item->id }}/0"><i class="fa fa-circle-o"></i>{{ substr($item->nombre,0,25) }}...</a></li>
              @endforeach
            </ul>
          </li>
          @endif

          @if (in_array($_SESSION['rol'], array(2)))
          <li class="treeview">
            <a href="#">
              <i class="fa fa-calendar"></i> <span>PRODUCTOS</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              @foreach (Categoria_productos::all()->where('estado', '1') as $item)
              <li><a href="/admin/reservacion/productos/{{ $item->id }}/0"><i class="fa fa-circle-o"></i>{{ substr($item->nombre,0,25) }}...</a></li>
              @endforeach
            </ul>
          </li>
          @endif

          @if (in_array($_SESSION['rol'], array(0,1,2)))
          <li class="treeview">
            <a href="#">
              <i class="fa fa-credit-card"></i> <span>PROFORMAS</span>
              <span class="pull-right-container">
                <i class="fa fa-angle-left pull-right"></i>
              </span>
            </a>
            <ul class="treeview-menu">
              @if (in_array($_SESSION['rol'], array(2)))
              <li><a href="/admin/reservacion/proforma"><i class="fa fa-circle-o"></i>GENERAR</a></li>
              @endif
              @if (in_array($_SESSION['rol'], array(0,1)))
              <li><a href="/admin/reservacion/proforma_cliente"><i class="fa fa-circle-o"></i>CLIENTES</a></li>
              @endif
            </ul>
          </li>
          @endif

          @if (in_array($_SESSION['rol'], array(0,1)))
          <li>
            <a href="/admin/reservacion/agenda">
              <i class="fa fa-calendar"></i> <span>AGENDA</span>
            </a>
          </li>
          @endif

          @if (in_array($_SESSION['rol'], array(0)))
          <li>
            <a href="/admin/respaldo">
              <i class="fa fa-cloud-download"></i> <span>RESPALDO</span>
            </a>
          </li>
          @endif
          
        </ul>
      </section>
      <!-- /.sidebar -->
    </aside>

    <!-- =============================================== -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">

      <!-- Main content -->
      <section class="content">
        
        @yield('content')

      </section>
      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->

    <footer class="main-footer">
      <div class="pull-right hidden-xs">
        <b>Creado por</b> Robin Ramos
      </div>
      <strong>Copyright &copy; 2018-2019
    </footer>

    <!-- Control Sidebar -->
    <aside class="control-sidebar control-sidebar-dark">
      <!-- Create the tabs -->
      <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
        <li><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
        <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
      </ul>
      <!-- Tab panes -->
      <div class="tab-content">
        <!-- Home tab content -->
        <div class="tab-pane" id="control-sidebar-home-tab">
          <h3 class="control-sidebar-heading">Recent Activity</h3>
          <ul class="control-sidebar-menu">
            <li>
              <a href="javascript:void(0)">
                <i class="menu-icon fa fa-birthday-cake bg-red"></i>

                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>

                  <p>Will be 23 on April 24th</p>
                </div>
              </a>
            </li>
            <li>
              <a href="javascript:void(0)">
                <i class="menu-icon fa fa-user bg-yellow"></i>

                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Frodo Updated His Profile</h4>

                  <p>New phone +1(800)555-1234</p>
                </div>
              </a>
            </li>
            <li>
              <a href="javascript:void(0)">
                <i class="menu-icon fa fa-envelope-o bg-light-blue"></i>

                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Nora Joined Mailing List</h4>

                  <p>nora@example.com</p>
                </div>
              </a>
            </li>
            <li>
              <a href="javascript:void(0)">
                <i class="menu-icon fa fa-file-code-o bg-green"></i>

                <div class="menu-info">
                  <h4 class="control-sidebar-subheading">Cron Job 254 Executed</h4>

                  <p>Execution time 5 seconds</p>
                </div>
              </a>
            </li>
          </ul>
          <!-- /.control-sidebar-menu -->

          <h3 class="control-sidebar-heading">Tasks Progress</h3>
          <ul class="control-sidebar-menu">
            <li>
              <a href="javascript:void(0)">
                <h4 class="control-sidebar-subheading">
                  Custom Template Design
                  <span class="label label-danger pull-right">70%</span>
                </h4>

                <div class="progress progress-xxs">
                  <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
                </div>
              </a>
            </li>
            <li>
              <a href="javascript:void(0)">
                <h4 class="control-sidebar-subheading">
                  Update Resume
                  <span class="label label-success pull-right">95%</span>
                </h4>

                <div class="progress progress-xxs">
                  <div class="progress-bar progress-bar-success" style="width: 95%"></div>
                </div>
              </a>
            </li>
            <li>
              <a href="javascript:void(0)">
                <h4 class="control-sidebar-subheading">
                  Laravel Integration
                  <span class="label label-warning pull-right">50%</span>
                </h4>

                <div class="progress progress-xxs">
                  <div class="progress-bar progress-bar-warning" style="width: 50%"></div>
                </div>
              </a>
            </li>
            <li>
              <a href="javascript:void(0)">
                <h4 class="control-sidebar-subheading">
                  Back End Framework
                  <span class="label label-primary pull-right">68%</span>
                </h4>

                <div class="progress progress-xxs">
                  <div class="progress-bar progress-bar-primary" style="width: 68%"></div>
                </div>
              </a>
            </li>
          </ul>
          <!-- /.control-sidebar-menu -->

        </div>
        <!-- /.tab-pane -->
        <!-- Stats tab content -->
        <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div>
        <!-- /.tab-pane -->
        <!-- Settings tab content -->
        <div class="tab-pane" id="control-sidebar-settings-tab">
          <form method="post">
            <h3 class="control-sidebar-heading">General Settings</h3>

            <div class="form-group">
              <label class="control-sidebar-subheading">
                Report panel usage
                <input type="checkbox" class="pull-right" checked>
              </label>
              <p>
                Some information about this general settings option
              </p>
            </div>
            <!-- /.form-group -->
            <div class="form-group">
              <label class="control-sidebar-subheading">
                Allow mail redirect
                <input type="checkbox" class="pull-right" checked>
              </label>
              <p>
                Other sets of options are available
              </p>
            </div>
            <!-- /.form-group -->
            <div class="form-group">
              <label class="control-sidebar-subheading">
                Expose author name in posts
                <input type="checkbox" class="pull-right" checked>
              </label>

              <p>
                Allow the user to show his name in blog posts
              </p>
            </div>
            <!-- /.form-group -->

            <h3 class="control-sidebar-heading">Chat Settings</h3>

            <div class="form-group">
              <label class="control-sidebar-subheading">
                Show me as online
                <input type="checkbox" class="pull-right" checked>
              </label>
            </div>
            <!-- /.form-group -->

            <div class="form-group">
              <label class="control-sidebar-subheading">
                Turn off notifications
                <input type="checkbox" class="pull-right">
              </label>
            </div>
            <!-- /.form-group -->

            <div class="form-group">
              <label class="control-sidebar-subheading">
                Delete chat history
                <a href="javascript:void(0)" class="text-red pull-right"><i class="fa fa-trash-o"></i></a>
              </label>
            </div>
            <!-- /.form-group -->
          </form>
        </div>
        <!-- /.tab-pane -->
      </div>
    </aside>
    <!-- /.control-sidebar -->
    <!-- Add the sidebar's background. This div must be placed
        immediately after the control sidebar -->
    <div class="control-sidebar-bg"></div>
  </div>
  <!-- ./wrapper -->

  <!-- jQuery 3 -->
  <script src="{{ base_url() }}assets/admin/bower_components/jquery/dist/jquery.min.js"></script>
  <!-- Bootstrap 3.3.7 -->
  <script src="{{ base_url() }}assets/admin/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- SlimScroll -->
  <script src="{{ base_url() }}assets/admin/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="{{ base_url() }}assets/admin/bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="{{ base_url() }}assets/admin/dist/js/adminlte.min.js"></script>
  <!-- AdminLTE for demo purposes -->
  <script src="{{ base_url() }}assets/admin/dist/js/demo.js"></script>
  
  <!-- izitoast -->
  <script src="{{ base_url() }}assets/admin/bower_components/izitoast/dist/js/iziToast.min.js"></script>
  <!-- InputMask -->
  <script src="{{ base_url() }}assets/admin/plugins/input-mask/jquery.inputmask.js"></script>
  <script src="{{ base_url() }}assets/admin/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
  <script src="{{ base_url() }}assets/admin/plugins/input-mask/jquery.inputmask.extensions.js"></script>
  <!-- date-range-picker -->
  <script src="{{ base_url() }}assets/admin/bower_components/moment/min/moment.min.js"></script>
  <script src="{{ base_url() }}assets/admin/bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <!-- bootstrap datepicker -->
  <script src="{{ base_url() }}assets/admin/bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <script src="{{ base_url() }}assets/admin/bower_components/bootstrap-datepicker/js/locales/bootstrap-datepicker.es.js" charset="UTF-8"></script>
  <!-- bootstrap time picker -->
  <script src="{{ base_url() }}assets/admin/plugins/timepicker/bootstrap-timepicker.min.js"></script>
  

  @yield('script')

</body>
</html>
