<!DOCTYPE html>
<html>
<head lang="es">
	<title>Producciones Ramos</title>
	<link href="<?= base_url() ?>assets/favicon.ico" rel="shortcut icon" type="image/vnd.microsoft.icon">
	<link href="<?php base_url() ?>assets/page/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
	<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
	<script src="<?php base_url() ?>assets/page/js/jquery.min.js"></script>
	<!-- Custom Theme files -->
	<!--theme-style-->
	<link href="<?php base_url() ?>assets/page/css/style.css" rel="stylesheet" type="text/css" media="all" />
	<!--//theme-style-->
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="keywords" content="Music Club Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
	Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
	<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
	<!--flexslider-->
	<link rel="stylesheet" href="<?php base_url() ?>assets/page/css/flexslider.css" type="text/css" media="screen" />
	<!--//flexslider-->
	@yield('style')
</head>
<body>
	<!--header-->
	<div class="header header-main">
		<div class="container">
			@include('include/nav')
		</div>
	</div>
	<!--//header-->
	<!--content-->
	@yield('content')
	<!--//content-->
	<!--footer-->
	@include('include/footer')
	<!--//footer-->
	@yield('script')
</body>
</html>