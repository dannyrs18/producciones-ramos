@php $pedido = Pedidos::where('cliente_id', $_SESSION['id'])->where('estado', '1')->first() @endphp
@if ($pedido)
<a href="javascript:void(0)" style="pointer-events: none;cursor: default;">
    <i class="fa fa-calendar"></i>
    {{ date("d/m/Y", strtotime($pedido->fecha_inicio)) }}@if ($pedido->fecha_fin) - {{ date("d/m/Y", strtotime($pedido->fecha_fin)) }} @endif
</a>
@else
<a href="javascript:pedido()" >
    <i class="fa fa-plus"></i>
    Generar
</a>
@endif

<script>
    function pedido(){
        iziToast.question({
            timeout: null,
            close: false,
            icon: 'fa fa-calendar',
            closeOnEscape: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            position: 'center',
            drag: false,
            inputs: [
                ['<div><label>Desde</label><input type="text" readonly id="inicio"></div>'],
                ['<div><label>Hasta:</label><span id="fecha_fin"> <button onclick="create_fecha_fin()">+</button></span></div>'],
            ],
            buttons: [
                ['<button><b>Confirmar</b></button>', function (instance, toast) {
                    fechas();
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>Cancelar</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
        
                }],
            ],
        });
        $('#inicio').datepicker({
            language: "es",
            startDate: '-0d',
            autoclose: true,
        })
        $("#inicio").on("change paste keyup", function() {
            delete_fecha_fin();
        });
        
    }

    function create_fecha_fin(){
        if ($('#inicio').val()!=""){
            $('#fecha_fin').html('<input type="text" class="datepicker" id="fin" readonly> <button onclick="delete_fecha_fin()">-</button>')

            var arregloFecha = $('#inicio').val().split("/");
            var anio = arregloFecha[2];
            var mes = arregloFecha[1];
            var dia = arregloFecha[0];

            var fecha = new Date(anio, mes, dia);
            fecha.setDate(fecha.getDate() + 1);
            $('#fin').datepicker({
                language: "es",
                autoclose: true,
                startDate: fecha
            })
        }else{

        }
    }

    function delete_fecha_fin(){
        $('#fecha_fin').html('<button onclick="create_fecha_fin()">+</button>')        
    }

    function fechas(){
        var inicio = $('#inicio').val();
        var fin = $('#fin').val();
        var error = false;
        var datos = {}
        if (inicio!= "" & typeof(fin) === 'undefined'){
            datos = {'inicio':inicio, 'tipo':0}
        }else if (inicio!= "" & fin != "") {
            if(fin>inicio){
                datos = {'inicio':inicio, 'fin':fin, 'tipo':1}
            }else{error=true}
        }else{error=true}
        if(error){
            iziToast.error({
                title: 'Error',
                message: 'Asigne las fechas correctamente',
            });
        }else{
            $.ajax({
                data: datos,
                url: "<?= base_url() ?>ajax/template_pedido",
                type: 'post',
                success: function(){
                    location.reload();
                }
            })
        }

    }
</script>