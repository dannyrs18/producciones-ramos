@php $pedido = Pedidos::where('cliente_id', $_SESSION['id'])->where('estado', '1')->first() @endphp
@if ($pedido)
<?php $proforma = Proformas::all()->where('pedido_id', $pedido->id) ?>
<a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <i class="fa fa-cart-plus"></i>
    <span class="label label-warning">{{ count($proforma) }}</span>
</a>
<ul class="dropdown-menu">
    <li class="header">{{ count($proforma) }} peticiones</li>
    <li>
        <!-- inner menu: contains the actual data -->
        <ul class="menu">
        @foreach ($proforma as $item)
        <li>
            <a href="#">
            @if ($item->producto())
            <i class="fa fa-users text-aqua"></i> {{ $item->cantidad }} {{ $item->producto()->nombre }}
            @else
            <i class="fa fa-users text-aqua"></i> {{ $item->cantidad }} {{ $item->servicio()->nombre }}
            @endif
            </a>
        </li>
        @endforeach
        </ul>
    </li>
    <li class="footer"><a href="/admin/reservacion/proforma">Revisar</a></li>
</ul>
@endif