<div class="header-top">
    <div class="logo">
        <h1><a href="inicio"><span style="color: teal">PROD</span>Ramos</a></h1>
    </div>
    <div class="top-nav">
        <span class="menu"><img src="<?php base_url() ?>assets/page/images/menu.png" alt=""> </span>
        <ul>
            <li><a href="inicio" class="hvr-sweep-to-bottom color"><i class="glyphicon glyphicon-home"></i>Inicio  </a> </li>
            <li><a href="album" class="hvr-sweep-to-bottom color1"><i class="glyphicon glyphicon-picture"></i>Galeria  </a> </li>
            <li><a href="blog"  class="hvr-sweep-to-bottom color2"><i class="glyphicon glyphicon-tags"></i>Localización</a></li>
            <li><a href="typo" class="hvr-sweep-to-bottom color3"><i class="glyphicon glyphicon-calendar"></i>Reservaciones </a></li>
            <li><a href="mail" class="hvr-sweep-to-bottom color4"><i class="glyphicon glyphicon-envelope"></i>Acerca </a></li>
            @if (!isset($_SESSION['username']))
            <li><a href="/page/login" class="hvr-sweep-to-bottom color5"><i class="glyphicon glyphicon-user"></i>Iniciar Sesión </a></li>
            <li><a href="/page/registro" class="hvr-sweep-to-bottom color6"><i class="glyphicon glyphicon-align-justify"></i>Registrate </a></li>
            @else
            <li><a href="/admin/inicio" class="hvr-sweep-to-bottom color6"><i class="glyphicon glyphicon-align-justify"></i>Administrar </a></li>
            @endif
            <div class="clearfix"> </div>
        </ul>
        <!--script-->
        <script>
            $("span.menu").click(function(){
                $(".top-nav ul").slideToggle(500, function(){
                });
            });
        </script>
    </div>
    <div class="clearfix"> </div>
</div>