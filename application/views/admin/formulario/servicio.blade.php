@extends('template/base_admin')

@section('content')
<div class="row">
    <!-- right column -->
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"> <strong>{{ $title }}</strong></h3>
            </div>
            <br>
            <!-- /.box-header -->
            <!-- form start -->
            @php
                $attr = array('class' =>"form-horizontal");
                echo form_open_multipart('', $attr);
            @endphp
              <div class="box-body">

                <div class="form-group">
                    <div class="col-sm-10 col-md-offset-2">
                        <?= validation_errors() ?>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="categoria" class="col-sm-2 control-label">Categoria</label>
                    <div class="col-sm-9">
                        <select class="form-control" name='categoria' required>
                        <option value selected>----------</option>
                        @foreach ($categoria as $item)
                        <option value="{{ $item->id }}" @if (set_value('categoria')==$item->id) @endif >{{ $item->nombre }}</option>
                        @endforeach
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label for="nombres" class="col-sm-2 control-label">Nombre</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" id="nombre" name="nombre" value="{{ set_value('nombre') }}" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="nombres" class="col-sm-2 control-label">Descripción</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="descripcion" id="descripcion" rows="3" required>{{ set_value('descripcion') }}</textarea>
                    </div>
                </div>

                <div class="form-group">
                    <label for="nombres" class="col-sm-2 control-label">Costo</label>
                    <div class="col-sm-9">
                        <input type="number" step="0.01" class="form-control" id="costo" name="costo" value="{{ set_value('costo') }}" required>
                    </div>
                </div>

                <div class="form-group">
                    <label for="nombres" class="col-sm-2 control-label">Imagen</label>
                    <div class="col-sm-9">
                        <input type="file" class="form-control" id="img" name="img">
                    </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="/admin/inicio" class="btn btn-default">Cancelar</a>
                <button type="submit" class="btn btn-info pull-right">Registrarse</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (right) -->
</div>
@endsection