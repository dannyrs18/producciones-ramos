@extends('template/base_admin')

@section('content')
<div class="row">
    <!-- right column -->
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"> <strong>{{ $title }}</strong></h3>
            </div>
            <br>
            <!-- /.box-header -->
            <!-- form start -->
            @php
                $attr = array('class' =>"form-horizontal");
                echo form_open_multipart('', $attr);
            @endphp
                <div class="box-body">

                    <div class="form-group">
                        <div class="col-sm-10 col-md-offset-2">
                            <?= validation_errors() ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="cedula" class="col-sm-2 control-label">Cedula</label>
                        <div class="col-sm-8">
                            <input type="number" id="cedula" class="form-control" name="cedula" value="{{ set_value('cedula') }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="nombres" class="col-sm-2 control-label">Nombres</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="nombres" name="nombres" value="{{ set_value('nombres') }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="apellidos" class="col-sm-2 control-label">Apellidos</label>
                        <div class="col-sm-8">
                            <input type="text" id="apellidos" class="form-control" name="apellidos" value="{{ set_value('apellidos') }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-8">
                            <input type="text" id="email" class="form-control" name="email" value="{{ set_value('email') }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="telefono" class="col-sm-2 control-label">Telefono</label>
                        <div class="col-sm-8">
                            <input type="number" min="0" id="telefono" class="form-control" name="telefono" value="{{ set_value('telefono') }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="edad" class="col-sm-2 control-label">Edad</label>
                        <div class="col-sm-8">
                            <input type="number" id="edad" min="0" class="form-control" name="edad" value="{{ set_value('edad') }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="direccion" class="col-sm-2 control-label">Dirección</label>
                        <div class="col-sm-8">
                            <textarea id="direccion" class="form-control" name="direccion" rows="2">{{ set_value('direccion') }}</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label">Genero</label>

                        <div class="col-sm-8">
                            <div class="radio">
                            <label>
                                <input type="radio" name="genero" id="optionsRadios1" value="1" >Hombre
                            </label>
                            <br>
                            <label>
                                <input type="radio" name="genero" id="optionsRadios1" value="0" >Mujer
                            </label>
                            </div>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="imagen" class="col-sm-2 control-label">Imagen de Perfil</label>
                        <div class="col-sm-8">
                            <input type="file" name="img" id="imagen">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="username" class="col-sm-2 control-label">Usuario</label>
                        <div class="col-sm-8">
                            <input type="text" id="username" class="form-control" name="username" value="{{ set_value('username') }}" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Contraseña</label>
                        <div class="col-sm-8">
                            <input type="password" id="password" class="form-control" name="password" required>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password2" class="col-sm-2 control-label">Repite la contraseña</label>
                        <div class="col-sm-8">
                            <input type="password" id="direccion" class="form-control" name="password2" required>
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="/admin/inicio" class="btn btn-default">Cancelar</a>
                    <button type="submit" class="btn btn-info pull-right">Registrarse</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (right) -->
</div>
@endsection