@extends('template/base_admin')

@section('content')
<div class="row">
    <!-- right column -->
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"> <strong>{{ $title }}</strong></h3>
            </div>
            <br>
            <!-- /.box-header -->
            <!-- form start -->
            @php
                $attr = array('class' =>"form-horizontal");
                echo form_open_multipart('', $attr);
            @endphp
                <div class="box-body">

                    <div class="form-group">
                        <div class="col-sm-8 col-md-offset-2">
                            <?= validation_errors() ?>
                        </div>
                    </div>
        
                    <div class="form-group">
                        <label for="nombres" class="col-sm-2 control-label">Nombres</label>
                        <div class="col-sm-8">
                            <input type="text" class="form-control" id="nombres" name="nombres" value="@if (set_value('nombres')){{ set_value('nombres') }}@else{{ $instance->nombres }}@endif">
                        </div>
                    </div>
        
                    <div class="form-group">
                        <label for="apellidos" class="col-sm-2 control-label">Apellidos</label>
                        <div class="col-sm-8">
                            <input type="text" id="apellidos" class="form-control" name="apellidos" value="@if (set_value('apellidos')){{ set_value('apellidos') }}@else{{ $instance->apellidos }}@endif">
                        </div>
                    </div>
        
                    <div class="form-group">
                        <label for="email" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-8">
                            <input type="text" id="email" class="form-control" name="email" value="@if (set_value('email')){{ set_value('email') }}@else{{ $instance->email }}@endif">
                        </div>
                    </div>
        
                    <div class="form-group">
                        <label for="telefono" class="col-sm-2 control-label">Telefono</label>
                        <div class="col-sm-8">
                            <input type="number" id="telefono" class="form-control" name="telefono" value="@if (set_value('telefono')){{ set_value('telefono') }}@else{{ $instance->telefono }}@endif">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="edad" class="col-sm-2 control-label">Edad</label>
                        <div class="col-sm-8">
                            <input type="number" id="edad" class="form-control" name="edad" value="@if (set_value('edad')){{ set_value('edad') }}@else{{ $instance->edad }}@endif">
                        </div>
                    </div>
        
                    <div class="form-group">
                        <label for="direccion" class="col-sm-2 control-label">Dirección</label>
                        <div class="col-sm-8">
                            <textarea id="direccion" class="form-control" name="direccion" rows="2">@if (set_value('direccion')){{ set_value('direccion') }}@else{{ $instance->direccion }}@endif</textarea>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="imagen" class="col-sm-2 control-label">Imagen de Perfil</label>
                        <div class="col-sm-8">
                            <input type="file" name="img" id="imagen">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Contraseña</label>
                        <div class="col-sm-8">
                            <input type="password" id="password" class="form-control" name="password">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password2" class="col-sm-2 control-label">Repite la contraseña</label>
                        <div class="col-sm-8">
                            <input type="password" id="password2" class="form-control" name="password2">
                        </div>
                    </div>

                </div>
                <!-- /.box-body -->
                <div class="box-footer">
                    <a href="/admin/inicio" class="btn btn-default">Cancelar</a>
                    <button type="submit" class="btn btn-info pull-right">Registrarse</button>
                </div>
                <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (right) -->
</div>
@endsection