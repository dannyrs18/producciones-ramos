@extends('template/base_admin')

@section('content')
<div class="row">
    <!-- right column -->
    <div class="col-md-12">
        <!-- Horizontal Form -->
        <div class="box box-info">
            <div class="box-header with-border">
                <h3 class="box-title"> <strong>{{ $title }}</strong></h3>
            </div>
            <br>
            <!-- /.box-header -->
            <!-- form start -->
            @php
                $attr = array('class' =>"form-horizontal");
                echo form_open('', $attr);
            @endphp
              <div class="box-body">

                <div class="form-group">
                    <div class="col-sm-10 col-md-offset-2">
                        <?= validation_errors() ?>
                    </div>
                </div>
                
                <div class="form-group">
                  <label for="nombres" class="col-sm-2 control-label">Nombre</label>
                  <div class="col-sm-8">
                    <input type="text" class="form-control" id="nombre" name="nombre" value="{{ set_value('nombre') }}" required>
                  </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                <a href="/admin/inicio" class="btn btn-default">Cancelar</a>
                <button type="submit" class="btn btn-info pull-right">Registrarse</button>
              </div>
              <!-- /.box-footer -->
            </form>
        </div>
        <!-- /.box -->
    </div>
    <!--/.col (right) -->
</div>
@endsection