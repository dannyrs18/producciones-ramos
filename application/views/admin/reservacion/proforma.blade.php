@extends('template/base_admin')

@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ base_url() }}assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title"> <strong>{{ $title }}</strong> </h3>
            </div>
            <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Cantidad</th>
                        <th>Categoria</th>
                        <th>Nombre</th>
                        <th>Unitario</th>
                        <th>Fecha</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    @php 
                        $total=0;
                        $pedido = Pedidos::where('estado', '1')->where('cliente_id', $_SESSION['id'])->first();
                    @endphp
                    @if ($pedido)
                    @foreach ($pedido->proformas() as $item)
                    <tr>
                        <td width="7%">{{ $item->cantidad }}</td>
                        @if ($item->producto())
                        <td>{{ $item->producto()->categoria_producto()->nombre }}</td>
                        <td>{{ $item->producto()->nombre }}</td>
                        <td>$ {{ money_format('%.2n', $item->producto()->costo) }}</td>
                        @else
                        <td>{{ $item->servicio()->categoria_servicio()->nombre }}</td>
                        <td>{{ $item->servicio()->nombre }}</td>
                        <td>$ {{ money_format('%.2n', $item->costo) }}</td>
                        @endif
                        <td>{{ $pedido->fecha_inicio }} @if ($pedido->fecha_fin) - {{ $pedido->fecha_fin }} @endif </td>
                        <td>$ {{ money_format('%.2n', $item->costo) }}</td>
                        @php $total=$total+$item->costo @endphp
                    </tr>
                    @endforeach
                        
                    @endif
                </tbody>
                <tfoot>
                    <tr>
                        <th>Total</th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th></th>
                        <th>$ {{ money_format('%.2n', $total) }}</th>
                    </tr>
                </tfoot>
            </table>            
        </div>
        <!-- /.box-body -->
        </div>
        <div class="col-md-12">
            <a href="/admin/reservacion/reporte/{{ $_SESSION['id'] }}" id="singlebutton" name="singlebutton" class="btn btn-success center-block"><i class="fa fa-file-pdf-o"></i> Imprimir</a>
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{ base_url() }}assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ base_url() }}assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
        })
    })
    </script>
@endsection
