@extends('template/base_admin')

@section('content')
<div class="row">
    <div class="col-xs-12">
        <div class="box box-primary">
            <div id="prueba">

            </div>
            <div class="box-header with-border">
                <h3 class="box-title"><strong>PRODUCTOS</strong></h3>
            </div>
            <!-- /.box-header -->

            <div class="box-body">
            <ul class="products-list product-list-in-box" id="products">
                @php $pedido = Pedidos::where('cliente_id', $_SESSION['id'])->where('estado', '1')->first() @endphp
                @foreach ($navigation as $item)
                <li class="item">
                    <div class="product-img">
                        @if ($item['img'])
                        <a href="/admin/producto/{{ $item['slug'] }}"><img src="{{ base_url() }}uploads/{{ $item['img'] }}" alt="Product Image" width="100"></a>
                        @else
                        <img src="{{ base_url() }}assets/admin/dist/img/gris.jpg" alt="Product Image" width="100">
                        @endif
                    </div>
                    <div class="product-info">
                        <a href="javascript:void(0)" class="product-title" style="padding-left: 12px;">{{ $item['nombre'] }}
                            <br>
                            @if ($pedido)
                                @if (in_array($item['id'], $proforma))
                                <button class="btn btn-ms btn-danger pull-right" onclick="eliminar_producto({{ $item['id'] }});"> <strong>-</strong> </button>
                                @endif
                                @if ($item['disponibilidad'])
                                <button class="btn btn-ms btn-success pull-right" onclick="agregar_producto({{ $item['id'] }});"> <strong>+</strong> </button>
                                @else
                                <button class="btn btn-ms btn-success pull-right" disabled> <strong>+</strong> </button>
                                @endif
                            @endif
                        </a>
                        <span class="product-description" style="padding-left: 12px;">
                            <span class="label label-warning" style="font-seliminar%">
                                $ {{ money_format('%.2n', $item['costo']) }} 
                            </span>
                            <br>
                            {{ $item['descripcion'] }}
                            <br>
                            Disponibilidad: <span id="disponibilidad">{{ $item['disponibilidad'] }}</span> 
                        </span>
                    </div>
                </li>
                @endforeach
            </ul>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-center">
                <div class="container">
                    <div class="row justify-content-md-center">
                        <div class="col-md-6 col-md-offset-3">
                            <div class="btn-toolbar" role="toolbar" aria-label="Toolbar with button groups">
                                <div class="btn-group mr-2" id="navigation" role="group" aria-label="First group">
                                    @for ($i = 0; $i < $total; $i++)
                                        @if ($i==$num)
                                            <a href="/admin/reservacion/productos/{{ $item['categoria_id'] }}/{{ $i }}" type="button" class="btn btn-primary">{{ $i+1 }}</a>
                                        @else
                                            <a href="/admin/reservacion/productos/{{ $item['categoria_id'] }}/{{ $i }}" type="button" class="btn btn-default">{{ $i+1 }}</a>
                                        @endif
                                    @endfor
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.box-footer -->
        </div>
    </div>
</div>
@endsection

@section('script')
<script>
    function agregar_producto(id){
        iziToast.success({
            timeout: null,
            close: false,
            icon: 'fa fa-check',
            closeOnEscape: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Advertencia',
            message: 'Esta seguro que desea agregar?',
            position: 'center',
            buttons: [
                ['<button><b>Aceptar</b></button>', function (instance, toast) {
                    add_producto(id);
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>Cancelar</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');        
                }],
            ],
        });
    }

    function add_producto(id){
        $.ajax({
            data: {'pedido': id, 'modelo':'producto', 'peticion':'agregar'},
            url: "<?= base_url() ?>ajax/template_proforma",
            type: 'post',
            success: function(data){
                location.reload();
            }
        })
    }

    function eliminar_producto(id){
        iziToast.error({
            timeout: null,
            close: false,
            icon: 'fa fa-close',
            closeOnEscape: true,
            overlay: true,
            displayMode: 'once',
            id: 'question',
            zindex: 999,
            title: 'Advertencia',
            message: 'Esta seguro que desea eliminar?',
            position: 'center',
            buttons: [
                ['<button><b>Aceptar</b></button>', function (instance, toast) {
                    del_producto(id);
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');
                }, true],
                ['<button>Cancelar</button>', function (instance, toast) {
                    instance.hide({ transitionOut: 'fadeOut' }, toast, 'button');        
                }],
            ],
        });
    }

    function del_producto(id){
        $.ajax({
            data: {'pedido': id, 'modelo':'producto', 'peticion':'eliminar'},
            url: "<?= base_url() ?>ajax/template_proforma",
            type: 'post',
            success: function(data){
                location.reload();
            }
        })
    }
</script>
@endsection