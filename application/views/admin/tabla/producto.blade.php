@extends('template/base_admin')

@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ base_url() }}assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title"> <strong>{{ $title }}</strong> </h3>
            </div>
            <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Categoria</th>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Costo</th>
                        <th>Disponibilidad</th>
                        <th>Estado</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach (Productos::all() as $item)
                    <tr>
                        <td>{{ $item->categoria_producto()->nombre }}</td>
                        <td>{{ $item->nombre }}</td>
                        <td>{{ $item->descripcion }}</td>
                        <td>{{ $item->costo }}</td>
                        <td>{{ $item->stock }}</td>
                        <td> @if ($item->estado) Activo @else Inactivo @endif </td>
                        <td>
                        @if ($item->estado)
                            <a href="/admin/producto/estado/{{ $item->slug }}" class="btn btn-danger btn-xs">Desactivar</a> 
                        @else
                            <a href="/admin/producto/estado/{{ $item->slug }}" class="btn btn-success btn-xs">Activar</a>    
                        @endif
                            <a href="/admin/producto/modificar/{{ $item->slug }}" class="btn btn-info btn-xs">Modificar</a>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Categoria</th>
                        <th>Nombre</th>
                        <th>Descripción</th>
                        <th>Costo</th>
                        <th>Disponibilidad</th>
                        <th>Estado</th>
                        <th>Opciones</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{ base_url() }}assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ base_url() }}assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
        })
    })
    </script>
@endsection
