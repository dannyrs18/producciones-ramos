@extends('template/base_admin')

@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ base_url() }}assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title"> <strong>Reservar</strong> </h3>
            </div>
            <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Cedula</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Direccion</th>
                        <th>Telefono</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach (Pedidos::all()->where('estado', '1') as $item)
                    <tr>
                        <td>{{ $item->cliente()->cedula }}</td>
                        <td>{{ $item->cliente()->nombres }}</td>
                        <td>{{ $item->cliente()->apellidos }}</td>
                        <td>{{ $item->cliente()->direccion }}</td>
                        <td>{{ $item->cliente()->telefono }}</td>
                        <td>
                            <a href="/admin/reservacion/reporte/{{ $item->cliente()->id }}" class="btn btn-success btn-xs">Proforma</a> 
                            <a href="/admin/reservacion/reservas/{{ $item->id }}" class="btn btn-warning btn-xs">Reserva</a> 
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Cedula</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Direccion</th>
                        <th>Telefono</th>
                        <th>Opciones</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<div class="row">
    <div class="col-xs-12">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title"> <strong>Reservados</strong> </h3>
            </div>
            <!-- /.box-header -->
        <div class="box-body">
            <table id="example2" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Cedula</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Direccion</th>
                        <th>Telefono</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach (Pedidos::all()->where('estado', '0') as $item)
                    <tr>
                        <td>{{ $item->cliente()->cedula }}</td>
                        <td>{{ $item->cliente()->nombres }}</td>
                        <td>{{ $item->cliente()->apellidos }}</td>
                        <td>{{ $item->cliente()->direccion }}</td>
                        <td>{{ $item->cliente()->telefono }}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Cedula</th>
                        <th>Nombres</th>
                        <th>Apellidos</th>
                        <th>Direccion</th>
                        <th>Telefono</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{ base_url() }}assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ base_url() }}assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
    $(function () {
        $('#example1').DataTable();
        $('#example2').DataTable();
    })
    </script>
    <script>
        function generar(){
            $.ajax({
                data: datos,
                url: "<?= base_url() ?>ajax/template_pedido",
                type: 'post',
                success: function(){
                    location.reload();
                }
            })
        }
    </script>
@endsection
