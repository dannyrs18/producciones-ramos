@extends('template/base_admin')

@section('style')
    <!-- DataTables -->
    <link rel="stylesheet" href="{{ base_url() }}assets/admin/bower_components/datatables.net-bs/css/dataTables.bootstrap.min.css">
@endsection

@section('content')
<div class="row">
    <div class="col-xs-12">

        <div class="box">
            <div class="box-header">
                <h3 class="box-title"> <strong>{{ $title }}</strong> </h3>
            </div>
            <!-- /.box-header -->
        <div class="box-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>Cedula</th>
                        <th>Apellidos</th>
                        <th>Nombres</th>
                        <th>Edad</th>
                        <th>Direccion</th>
                        <th>Telefono</th>
                        <th>Email</th>
                        <th>Estado</th>
                        <th>Opciones</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach (Usuarios::all()->where('rol', '1') as $item)
                    <tr>
                        <td>{{ $item->cedula }}</td>
                        <td>{{ $item->apellidos }}</td>
                        <td>{{ $item->nombres }}</td>
                        <td>{{ $item->edad }}</td>
                        <td>{{ $item->direccion }}</td>
                        <td>{{ $item->telefono }}</td>
                        <td>{{ $item->email }}</td>
                        <td> @if ($item->estado) Activo @else Inactivo @endif </td>
                        <td>
                        @if ($item->estado)
                            <a href="/admin/usuario/estado/{{ $item->slug }}" class="btn btn-danger btn-xs">Desactivar</a> 
                        @else
                            <a href="/admin/usuario/estado/{{ $item->slug }}" class="btn btn-success btn-xs">Activar</a>    
                        @endif
                        </td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>Cedula</th>
                        <th>Apellidos</th>
                        <th>Nombres</th>
                        <th>Edad</th>
                        <th>Direccion</th>
                        <th>Telefono</th>
                        <th>Email</th>
                        <th>Estado</th>
                        <th>Opciones</th>
                    </tr>
                </tfoot>
            </table>
        </div>
        <!-- /.box-body -->
        </div>
        <!-- /.box -->
    </div>
    <!-- /.col -->
</div>
<!-- /.row -->
@endsection

@section('script')
    <!-- DataTables -->
    <script src="{{ base_url() }}assets/admin/bower_components/datatables.net/js/jquery.dataTables.min.js"></script>
    <script src="{{ base_url() }}assets/admin/bower_components/datatables.net-bs/js/dataTables.bootstrap.min.js"></script>
    <script>
    $(function () {
        $('#example1').DataTable()
        $('#example2').DataTable({
        'paging'      : true,
        'lengthChange': false,
        'searching'   : false,
        'ordering'    : true,
        'info'        : true,
        'autoWidth'   : false
        })
    })
    </script>
@endsection
