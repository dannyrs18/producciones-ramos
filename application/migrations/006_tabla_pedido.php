<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Tabla_pedido extends CI_Migration {

    public function up(){

        $this->dbforge->add_field(array(
            'id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'auto_increment' => TRUE),
            'total' => array('type' => 'FLOAT', 'default' => '0.00'),
            'num_pedido' => array('type' => 'VARCHAR', 'constraint' => 40),
            'estado' => array('type' => 'ENUM("0","1")', 'default' => '1'),
            'cliente_id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE),
            'fecha_inicio' => array('type' => 'DATE'),
            'fecha_fin' => array('type' => 'DATE', 'null' => TRUE),
            'updated_at' => array('type' => 'TIMESTAMP'),
            'created_at' => array('type' => 'TIMESTAMP'),
            )
        );
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("CONSTRAINT FOREIGN KEY (cliente_id) REFERENCES usuario(id)");
        $this->dbforge->create_table('pedido', TRUE, ['ENGINE' => 'InnoDB']);
    }

    public function down(){
        $this->dbforge->drop_table('pedido');
    }
}