<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Tabla_proforma extends CI_Migration {

    public function up(){

        $this->dbforge->add_field(array(
            'id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'auto_increment' => TRUE),
            'costo' => array('type' => 'FLOAT'),
            'cantidad' => array('type' => 'INT', 'default' => 1),
            'producto_id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'null' => TRUE),
            'servicio_id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'null' => TRUE),
            'pedido_id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'null' => TRUE),
            'updated_at' => array('type' => 'TIMESTAMP'),
            'created_at' => array('type' => 'TIMESTAMP'),
            )
        );
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("CONSTRAINT FOREIGN KEY (pedido_id) REFERENCES pedido(id)");
        $this->dbforge->add_field("CONSTRAINT FOREIGN KEY (producto_id) REFERENCES producto(id)");
        $this->dbforge->add_field("CONSTRAINT FOREIGN KEY (servicio_id) REFERENCES servicio(id)");
        $this->dbforge->create_table('proforma', TRUE, ['ENGINE' => 'InnoDB']);
    }

    public function down(){
        $this->dbforge->drop_table('proforma');
    }
}   