<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Tabla_categoria_servicio extends CI_Migration {

    public function up(){
        $this->dbforge->add_field(array(
            'id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'auto_increment' => TRUE),
            'nombre' => array('type' => 'VARCHAR', 'constraint' => 100),
            'estado' => array('type' => 'ENUM("0","1")', 'default' => '1'),
            'slug' => array('type' => 'VARCHAR', 'constraint' => 40),
            'updated_at' => array('type' => 'TIMESTAMP'),
            'created_at' => array('type' => 'TIMESTAMP'),
            )
        );
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('categoria_servicio');
    }

    public function down(){
        $this->dbforge->drop_table('categoria_servicio');
    }
}