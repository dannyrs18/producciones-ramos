<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Tabla_servicio extends CI_Migration {

    public function up(){

        $this->dbforge->add_field(array(
            'id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'auto_increment' => TRUE),
            'costo' => array('type' => 'FLOAT'),
            'descripcion' => array('type' => 'TEXT'),
            'nombre' => array('type' => 'VARCHAR', 'constraint' => 40, 'unique' => TRUE),
            'categoria_id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE),
            'img' => array('type' => 'TEXT', 'null' => TRUE),
            'estado' => array('type' => 'ENUM("0","1")', 'default' => '1'),
            'slug' => array('type' => 'VARCHAR', 'constraint' => 40),
            'updated_at' => array('type' => 'TIMESTAMP'),
            'created_at' => array('type' => 'TIMESTAMP'),
            )
        );
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->add_field("CONSTRAINT FOREIGN KEY (categoria_id) REFERENCES categoria_servicio(id)");
        $this->dbforge->create_table('servicio', TRUE, ['ENGINE' => 'InnoDB']);
    }

    public function down(){
        $this->dbforge->drop_table('servicio');
    }
}