<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Migration_Tabla_usuario extends CI_Migration {

    public function create_user(){
        $data['cedula'] = '1150188090';
        $data['apellidos'] = 'Ramos';
        $data['nombres'] = 'Robin';
        $data['direccion'] = 'Centenario';
        $data['email'] = 'dromystyle96@gmail.com';
        $data['genero'] = '1';
        $data['telefono'] = '0959576573';
        $data['edad'] = 22;
        $data['img'] = '';
        $data['username'] = 'robinrs';
        $data['password'] = 'robin2018';
        $data['rol'] = '0';
        $this->load->model('modelos');
        $this->modelos->usuario($data);
    }

    public function up(){
        $this->dbforge->add_field(array(
                'id' => array('type' => 'BIGINT', 'constraint' => 20, 'unsigned' => TRUE, 'auto_increment' => TRUE),
                'cedula' => array('type' => 'VARCHAR', 'constraint' => 12, 'unique' => TRUE),
                'apellidos' => array('type' => 'VARCHAR', 'constraint' => 180),
                'nombres' => array('type' => 'VARCHAR', 'constraint' => 180),
                'direccion' => array('type' => 'TEXT'),
                'email' => array('type' => 'VARCHAR', 'constraint' => 200),
                'genero' => array('type' => 'ENUM("0","1")'),
                'telefono' => array('type' => 'VARCHAR', 'constraint' => 13),
                'edad' => array('type' => 'INT', 'constraint' => 11),
                'estado' => array('type' => 'ENUM("0","1")', 'default' => '1'),
                'img' => array('type' => 'TEXT', 'null' => TRUE),
                'username' => array('type' => 'VARCHAR', 'constraint' => 20, 'unique' => TRUE),
                'password' => array('type' => 'VARCHAR', 'constraint' => 60),
                'rol' => array('type' => 'ENUM("0","1","2")'), // 0->ADMINISTRADOR, 1->USUARIO, 2->CLIENTE
                'slug' => array('type' => 'VARCHAR', 'constraint' => 40),
                'updated_at' => array('type' => 'TIMESTAMP'),
                'created_at' => array('type' => 'TIMESTAMP'),
            )
        );
        $this->dbforge->add_key('id', TRUE);
        $this->dbforge->create_table('usuario', TRUE, ['ENGINE' => 'InnoDB']);
        $this->create_user();
    }

    public function down(){
        $this->dbforge->drop_table('usuario');
    }
}